var site = "http://ecat.elit.ro",
    user = "andreinancu_wb",
    password = 'cantacuzino13_wb',
    did = 4,
    total_runs = 2950;

var file = '/tmp/elit.lock';

var Nightmare = require('nightmare'),
    fs = require('fs'),
    random = require('random-js'),
    Sequelize = require('sequelize'),
    equiv = require(__dirname + '/equiv/manufacturers'),
    vo = require('vo'),
    cheerio = require('cheerio'),
    mkdirp = require('mkdirp'),
    nconf = require('nconf'),
    foreach = require('generator-foreach'),
    path = require('path'),
    mv = require('mv');

if (fs.existsSync(file))
    process.exit(1);

fs.closeSync(fs.openSync(file, 'w'));

nconf.argv()
    .env()
    .file({file: __dirname + '/config.json'});

var times_run = nconf.get('task:run');

if (times_run > total_runs) {
    if (fs.existsSync(file)) {
        fs.unlink(file);
    }
    process.exit(1);
}

var db = nconf.get('database');

var scraper = nconf.get('scraper'),
    connection = new Sequelize(db.name, db.user, db.password, {
        host: db.host,
        dialect: db.dialect,
        logging: false
    }),
    models = require('sequelize-import')(__dirname + '/mysql_models', connection),
    product_queue = [];

models.Product.hasMany(models.DistP, {foreignKey: 'product_id', constraints: false});
models.Product.hasOne(models.Elit, {foreignKey: 'product_id'});
models.Elit.belongsTo(models.Product, {foreignKey: 'id'});

models.Product.findAll({
    include: [{
        model: models.DistP,
        attributes: [],
        where: {distrib_id: did},
    }],
    order: Sequelize.literal('DistPs.created_at DESC')
}).then(function (products) {
    products.forEach(function (product) {
        models.Elit.findOne({where: {product_id: product.id}}).then(function (elit) {
            if (elit == null) {
                var seconds = random.integer(5000, 20000)(random.engines.nativeMath);

                var obj = {
                    id: product.product_id,
                    model: product.model,
                    manufacturer_id: product.manufacturer_id,
                    sku: product.model,
                    seconds: seconds
                };
                product_queue.push(obj);
            }
        });
    });
});

models.DistM.findAll({attributes: ['manufacturer_id'], where: {distrib_id: did}}).then(function (distms) {
    var m = [];
    distms.forEach(function (distm) {
        equiv.manufacturers.forEach(function (ms) {
            if (ms.id == distm.manufacturer_id)
                m.push(ms);
        });
    });
    return m;

}).done(function (manufacturers) {
    manufacturers.forEach(function (manufacturer) {
        models.Product.findAll({
            order: [
                Sequelize.fn('RAND')
            ],
            limit: scraper.limit,
            where: {manufacturer_id: manufacturer.id}
        }).then(function (products) {
            products.forEach(function (product) {
                models.Elit.findOne({where: {product_id: product.id}}).then(function (elit) {

                    if (elit == null) {
                        var seconds = random.integer(5000, 20000)(random.engines.nativeMath);

                        var obj = {
                            id: product.product_id,
                            model: product.model,
                            manufacturer_id: product.manufacturer_id,
                            sku: product.model,
                            seconds: seconds
                        };
                        product_queue.push(obj);

                    }
                });
            });
        });
    });
});

function* run() {
    var nightmare = Nightmare({
        width: 1280,
        height: 1024,
    });

    yield nightmare
        .goto(site)
        .wait()
        .type('#ctl00_ContentPlaceHolder1_LoginControl_tbLoginUser', user)
        .wait()
        .type('#ctl00_ContentPlaceHolder1_LoginControl_tbLoginPass', password)
        .wait()
        .click('#ctl00_ContentPlaceHolder1_LoginControl_btnLogin')
        .wait('#ctl00_cphMenu_MenuControl_tbSearch')
        .screenshot(__dirname + '/status.png');

    yield * foreach(product_queue, function* (p, k) {

        times_run++;

        nconf.set('task:run', times_run);

        nconf.save(function (err) {
            fs.readFile(__dirname + '/config.json', function (err, data) {
                console.dir(JSON.parse(data.toString()))
            });
        });

        yield nightmare
            .type('#ctl00_cphMenu_MenuControl_tbSearch', p.sku)
            .click('#ctl00_cphMenu_MenuControl_imgArrowSearch');

        yield nightmare.wait('#ctl00_cphMenu_MenuControl_tbSearch');

        var next_page = true;

        // while (next_page) {
        var content = yield nightmare
            .wait(p.seconds)
            .evaluate(function () {
                return document.querySelector("body").innerHTML;
            });

        proc(p.id, p.sku, p.manufacturer, p.manufacturer_id, content);

        yield nightmare.screenshot(__dirname + '/current.png');

        /*   next_page = yield nightmare
         .evaluate(function () {

         if(!$('#ctl00_cphBody_ArticleBrowserCtl_apcPager_btnNext').is(':disabled'))
         return false;
         else
         return true;
         });

         if (next_page) {
         yield nightmare
         .click('#ctl00_cphBody_ArticleBrowserCtl_apcPager_btnNext')
         .wait('#ctl00_cphMenu_MenuControl_tbSearch');
         }

         }
         */

        if (times_run > total_runs) {
            if (fs.existsSync(file)) {
                fs.unlink(file);
            }
            process.exit(1);
        }
    });

    if (fs.existsSync(file))
        fs.unlink(file);

    yield nightmare.end();
}

function addProduct(elit_code, product_id, sku, manufacturer_id, price, stock, manufacturer) {


    models.Elit.findOrCreate({
        where: {
            elit_code: elit_code
        },
        defaults: {
            elit_code: elit_code,
            manufacturer: manufacturer,
            price: price,
            stock: stock
        }
    }).spread(function (product) {
        product.update({
            price: price,
            stock: stock,
            updated: 1
        }, {fields: ['price', 'stock', 'updated']});
    });
}

function proc(product_id, sku, manufacturer, manufacturer_id, data) {


    if (data != '') {
        $ = cheerio.load(data);

        if ($('.article-browser tr').length) {
            var i = 0;
            $('.article-browser tr').each(function (k, v) {
                if (i != 0) {
                    var code = $(v).find('td:nth-of-type(1) a').text();
                    var price = $(v).find('td:nth-of-type(5) span:nth-of-type(1)').text().replace(" RON", "").replace(' ', '').replace(',', '.').trim();
                    var s = $(v).find('td:nth-of-type(4) span').text().trim();
                    var stock = 0;

                    if (s != 'Informaţii la cerereInformaţii la cerere' && s != 'Verificare disponibilitate')
                        stock = 1;

                    var man = $(v).find('td:nth-of-type(3)').text();

                    if (code != '' && price != '' && man != stock)
                        addProduct(code, 0, "", 0, price, s, man);

                }
                i++;
            });
        }
        /*
         else
         addProduct('', product_id, sku, manufacturer_id, 0, 0, manufacturer);
         */

    }
}

vo(run)(function (err, result) {
    if (err) throw err;
});
