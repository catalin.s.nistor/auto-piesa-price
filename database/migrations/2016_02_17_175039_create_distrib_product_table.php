<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistribProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distrib_product', function (Blueprint $table)
        {
            $table->integer('distrib_id')->unsigned();
            $table->integer('product_id')->unsigned();

            $table->foreign('distrib_id')
                ->references('id')
                ->on('distrib');

            $table->foreign('product_id')
                ->references('id')
                ->on('products');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('distrib_product');
    }
}
