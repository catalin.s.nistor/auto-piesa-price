<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConexPrice extends Model
{

    //public $timestamps = false;

    protected $fillable = ['product_id', 'model', 'manufacturer_id'];

    public function product() {
        $this->belongsTo('App\Product');
    }


    public function getPriceAttribute($value)
    {
        return tofloat($value);
    }
}
