@extends('app')

@section('content')

    <div class="row">


        <div class="col-md-6">
            <h3>Pasul 1</h3>
            <h4>Download Lista Cautare Intercars</h4>

            <div class="form-group">
                {!! Form::label('Status:') !!}
                <p class="status">Nu este generat</p>
            </div>


            <div class="form-group">
                <button class="btn btn-primary form-control get_list">Genereaza</button>
            </div>


        </div>

        <div class="col-md-6">
            <h3>Pasul 2</h3>
            <h4>Upload Lista de preturi Intercars</h4>

            {!! Form::open([
                'action' => 'UploadIntercarsController@store',
                'files' => 'true'])
             !!}

            <div class="form-group">
                {!! Form::label('Fisier CSV Intercars:') !!}
                {!! Form::file('file', null) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Trimite', ['class' => 'btn btn-primary form-control']) !!}
            </div>

            {!! Form::close()  !!}

            @if($errors->any())

                <ul class="alert alert-danger">
                    <li>Incarcati un fisier de tip csv</li>
                </ul>

            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

        </div>

    </div>

    <div class="row">
        <div class="col-md-12">
            <h3>Status Masina Virtuala Windows 7</h3>
            <div class="row">
                <div class="col-md-2">
                    <div class="input-group">
                     <span class="input-group-btn">
                         <span class="btn btn-default vmstatus">OFF</span>
                        <button class="btn btn-success vmstart"><span class="glyphicon glyphicon-play"
                                                                      aria-hidden="true"></span> Start
                        </button>
                        <button class="btn btn-danger vmstop"><span class="glyphicon glyphicon-stop"
                                                                    aria-hidden="true"></span> Stop
                        </button>
                         <!--
                        <button class="btn btn-warning vmrestart"><span class="glyphicon glyphicon-refresh"

                        </button>
                                                                                        aria-hidden="true"></span> Restart
                                                                                             -->

                        <button class="btn btn-primary spice-address"><span
                                    class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Vizualieaza
                        </button>
                    </span>
                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection

@section('footer')
    <script type="text/javascript">

        $(document).ready(function () {
                    get_vmstatus();
                    get_status();

                    function copytext(text) {
                        var textField = document.createElement('textarea');
                        textField.innerText = text;
                        document.body.appendChild(textField);
                        textField.select();
                        document.execCommand('copy');
                        textField.remove();
                    }

                    $('.spice-address').click(function (e) {
                        var text = "spice://{!! $_SERVER['SERVER_NAME'] !!}?port=5900";
                        copytext(text);
                        alert('Adresa pentru remote viewer a fost copiata in memorie!\n\nAdresa este: \n\n' + text);
                    });


                    $.post('{{ url("intercars/status") }}', {get_list: true}).done(function (data) {
                        if (data != '') {
                            $(".status").html('<a href="' + data + '" download>' + data + "</a>");
                        }
                    });


                    function is_off() {
                        $(".vmstatus").html("OFF");
                        $('.vmstop').addClass('disabled');
                        $('.spice-address').addClass('disabled');
                        $('.vmstart').removeClass('disabled');
                    }

                    function is_on() {
                        $(".vmstatus").html("ON");
                        $('.vmstart').addClass('disabled');
                        $('.vmstop').removeClass('disabled');
                        $('.spice-address').removeClass('disabled');

                    }

                    function get_status() {
                        var refreshIntervalId = setInterval(function () {
                            $.post('{{ url("intercars/status") }}', {get_list: true}).done(function (data) {
                                if (data != '') {
                                    clearInterval(refreshIntervalId);
                                    $(".status").html('<a href="' + data + '" download>' + data + "</a>");
                                }
                            });
                        }, 2000);
                    }

                    function get_vmstatus() {
                        $.post('{{ url("intercars/vmstatus") }}', {status: true}).done(function (data) {
                            if (data == 1) {
                                is_on();

                            }
                            if (data == 0) {
                                $(".vmstatus").html("OFF");
                                is_off();

                            }
                        });
                    }

                    $('.get_list').click(function (e) {
                        $.post('{{ url("intercars/download") }}', {get_list: true});
                        $(".status").html('In curs de generare' + '  {!!  Html::image('/images/loading.gif', 'alt', [ 'width' => 16, 'height' => 16 ]) !!}');
                        get_status();
                    });

                    $('.vmstop').click(function (e) {
                        e.preventDefault();
                        if (!$(this).hasClass('disabled')) {
                            var c = confirm("Atentie! Masina virtuala se va opri fortat! Se recomanda folosirea butonului SHUT DOWN din Windows 7! Sunteti sigur?");
                            if (c === true) {
                                $.post('{{ url("intercars/vmstop") }}', {stop: true});
                                is_off();
                            }
                        }
                    });

                    $('.vmstart').click(function (e) {
                        e.preventDefault();
                        if (!$(this).hasClass('disabled')) {
                            $.post('{{ url("intercars/vmstart") }}', {start: true});
                            is_on();
                        }
                    });

                    $('.vmrestart').click(function (e) {
                        $.post('{{ url("intercars/vmrestart") }}', {restart: true});
                    });

                    setInterval(function () {
                        get_vmstatus();
                    }, 5000);
                }
        );

    </script>


@endsection