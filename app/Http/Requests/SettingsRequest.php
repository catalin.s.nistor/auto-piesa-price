<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SettingsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'l50' => 'required|min:0|max:100',
            'b50_100' => 'required|min:0|max:100',
            'b100_200' => 'required|min:0|max:100',
            'b200_500' => 'required|min:0|max:100',
            'g500' => 'required|min:0|max:100',
            'ulei' => 'required|min:0|max:100',
            'baterii' => 'required|min:0|max:100',
            'lichide' => 'required|min:0|max:100'
        ];
    }
}
