<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BennettPrice extends Model
{
    //public $timestamps = false;

    protected $guarded = [];

    public function product() {
        $this->belongsTo('App\Product');
    }

}
