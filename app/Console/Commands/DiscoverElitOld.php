<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artisaninweb\SoapWrapper\Facades\SoapWrapper;

class DiscoverElit extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discover:elit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Elit distributor product disvovery.';

    /**
     * Client
     *
     * @var \SoapClient
     */
    protected $client;

    /**
     * Username
     *
     * @var string
     */
    protected $login = 'andreinancu_wb';

    /**
     * Password
     *
     * @var string
     */
    protected $password = 'cantacuzino13_wb';

    /**
     * Manufacturers
     *
     * @var array
     */
    protected $manufacturers = [
        20  => '',
        25  => '',
        106 => 'S',
        49  => 'V',
        124 => 'PI',
        81  => 'GR',
        13  => '',
        120 => 'FA',
        30  => '',
        202 => 'TES',
        128 => '',
        51  => 'SI',
        77  => '',
        123 => 'COF',
        155 => 'OSR',
        104 => 'AIR',
        119 => '',
        92  => 'CO',
        166 => 'MSP',
        40  => 'MAP',
        38  => '',
        21  => '',
        36  => 'LMI',
        82  => ['0.0', 'SPIDAN', 'GKN'],
        110 => '',
        112 => '',
        11  => '',
        78  => '',
        165 => 'FIL',
        113 => 'A',
        173 => '',
        135 => 'WH',
        121 => 'MTS',
        95  => '',
        29  => 'KYB',
        43  => 'AL',
        162 => 'EB',
        179 => '',
        109 => '',
        60  => 'SDM',
        200 => 'RH',
        54  => 'T',
        94  => 'BRE',
        161 => 'KN',
        126 => '',
        90  => 'DAY',
        87  => 'EL',
        125 => '',
        197 => 'NIP',
        91  => 'DEN',
        111 => '',
        146 => 'GOM',
    ];

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $wsdl = 'http://icelit02.elit.cz:7606/InterCompany-1.10.0/BuyerService?wsdl';
        $this->client = new \SoapClient($wsdl);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->listElitMan();

        /* $manufacturers = array_keys($this->manufacturers);


         \App\Product::orderByRaw('RAND()')
             ->select(["products.*", "elit_prices.product_id"])
             ->whereIn('manufacturer_id', $manufacturers)
             ->leftJoin("elit_prices", 'products.id', '=', 'elit_prices.product_id')
             ->where('elit_prices.product_id', null)
             ->chunk(100, function ($p)
             {
                 foreach ($p as $product)
                 {

                     $elit = \App\ElitPrice::firstOrNew(['product_id' => $product->id]);
                     $elit->save();


                     if(!is_array($this->manufacturers[$product->manufacturer_id]))
                     {
                         $product_code = trim($this->manufacturers[$product->manufacturer_id] . ' ' . $product->model);
                         $resp = $this->getProduct($product_code);

                         if($resp->retailPrice != 0)
                         {
                             $this->setProduct($product, $resp, $elit);
                             continue;
                         }

                         $product_code = trim($this->manufacturers[$product->manufacturer_id] . ' ' . str_replace(' ', '', $product->model));
                         $resp = $this->getProduct($product_code);

                         if($resp->retailPrice != 0)
                         {
                             $this->setProduct($product, $resp, $elit);
                             continue;
                         }

                         $product_code = trim($this->manufacturers[$product->manufacturer_id] . ' ' . str_replace(['-', ' '], '.', $product->model));
                         $resp = $this->getProduct($product_code);

                         if($resp->retailPrice != 0)
                         {
                             $this->setProduct($product, $resp, $elit);
                             continue;
                         }
                     } else
                         foreach ($this->manufacturers[$product->manufacturer_id] as $m)
                         {

                             $product_code = trim($m . ' ' . $product->model);
                             $resp = $this->getProduct($product_code);

                             if($resp->retailPrice != 0)
                             {
                                 $this->setProduct($product, $resp, $elit);
                                 break;
                             }

                             $product_code = trim($m . ' ' . str_replace(' ', '', $product->model));
                             $resp = $this->getProduct($product_code);

                             if($resp->retailPrice != 0)
                             {
                                 $this->setProduct($product, $resp, $elit);
                                 break;
                             }

                             $product_code = trim($m . ' ' . str_replace(['-', ' '], '.', $product->model));
                             $resp = $this->getProduct($product_code);

                             if($resp->retailPrice != 0)
                             {
                                 $this->setProduct($product, $resp, $elit);
                                 break;
                             }
                         }
                 }
             });
 */

    }

    private function getProduct($model)
    {

        try
        {
            $resp = $this->client->getItemInfo(
                [
                    'company'  => "ELIT_RO",
                    'login'    => $this->login,
                    'password' => $this->password,
                    'itemNo'   => $model
                ]
            );

            return $resp->return;

        } catch (Exception $e)
        {
            echo $e->getMessage() . "\n\n\n";

            return false;
        }

    }

    private function setProduct($product, $resp, $elit)
    {

        $elit->price = 0;

        if($resp->discountPct != 0)
            $elit->price = round(round($resp->retailPrice - ($resp->retailPrice * $resp->discountPct / 100), 2) * 1.24, 2);


        $stock = 0;
        if(isset($resp->stock) && is_array($resp->stock))
        {
            foreach ($resp->stock as $s)
                $stock += intval($s->quantity);

            $elit->stock = $stock;
        }

        $elit->save();

        if($stock > 0)
        {
            \App\ProductHasPrice::firstOrCreate(['product_id' => $product->id]);
        }
    }

    private function listElitMan()
    {
        foreach ($this->manufacturers as $k => $v)
        {
            $manufacturer = \App\Manufacturer::findOrNew($k);
            echo "{id: " . $k . ", name: '" . $manufacturer->name . "'},\n";
        }

    }

}