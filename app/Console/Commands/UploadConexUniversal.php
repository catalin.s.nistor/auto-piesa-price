<?php

namespace App\Console\Commands;

use DB;
use File;
use Mockery\CountValidator\Exception;
use Storage;
use Illuminate\Console\Command;
use anlutro\cURL\cURL;


class UploadConexUniversal extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:conex_universal';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Conex universal products';

    /**
     * @var curl
     */
    protected $curl;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(cURL $cURL)
    {
        $this->curl = $cURL;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $settings = \App\Settings::lists('value', 'key');

        $imgs = [];

        if(Storage::disk('node')->get('conex/universal.json'))
        {
            $contents = Storage::disk('node')->get('conex/universal.json');


            foreach (json_decode($contents) as $code => $obj)
            {

                // if($obj->stock > 0)
                // {
                $imgs[] = $obj->image;
                $extension = pathinfo($obj->image, PATHINFO_EXTENSION);
                $file_name = pathinfo($obj->image, PATHINFO_FILENAME);


                $add = ($settings['ulei'] / 100) + 1;


                $manufacturer = \App\Manufacturer::firstOrCreate(['name' => $obj->manufacturer]);


                $product = \App\Product::firstOrCreate(
                    [
                        'model'           => $code,
                        'manufacturer_id' => $manufacturer->id,
                    ]
                );

                $price = tofloat($obj->price);

                $product->price = ceil($price * 1.24 * $add);

                $product->quantity = $obj->stock;
                $product->location = 'CONEX';
                $product->image = 'data/PRODUSE_UNIVERSALE/' . $file_name . '.' . $extension;

                $product->categories()->sync([165]);
                $product->save();

                $pd = \App\ProductDescription::firstOrNew(['id' => $product->id]);
                $pd->name = $obj->name;
                $pd->save();

/*
                if($obj->stock > 0)
                    $query = "UPDATE oc_product SET image ='" . $product->image . "', price ='" . $price_up . "', location ='" . $product->location . "', quantity=100  WHERE model ='" . $code . "' AND manufacturer_id = " . $manufacturer->id . " ;";

                else
                    $query = "UPDATE oc_product SET image ='" . $product->image . "', price ='" . $price_up . "', location ='" . $product->location . "', quantity=0, stock_status_id=5  WHERE model ='" . $code . "' AND manufacturer_id = " . $manufacturer->id . " ;";

                ///  Storage::disk('local')->append('new.sql', $query);
*/
            }
            /*

            foreach ($imgs as $img)
            {
                $extension = pathinfo($img, PATHINFO_EXTENSION);
                $file_name = pathinfo($img, PATHINFO_FILENAME);
                $file = file_get_contents($img);
                sleep(1);
                Storage::disk('local')->put('img/' . $file_name . '.' . $extension, $file);
                sleep(1);
               // Storage::disk('ftp')->put($file_name . '.' . $extension, $file);
            }
*/

        }
    }
}
