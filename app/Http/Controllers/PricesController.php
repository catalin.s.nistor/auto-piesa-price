<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductDistRequest;
use App\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use yajra\Datatables\Datatables;
use yajra\Datatables\Html\Builder; // import class on controller
use App\Http\Requests\SettingsRequest;

class PricesController extends Controller {



    protected $htmlbuilder;
    protected $distribs;

    /**
     * @param Builder $htmlBuilder
     */

    public function __construct(Builder $htmlBuilder)
    {
        $this->htmlBuilder = $htmlBuilder;
        $this->middleware('auth');


    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     *
     * display products
     */

    public function index(Request $request)
    {

        if($request->ajax())
        {

            $products = \App\Product::leftJoin('product_description', 'products.id', '=', 'product_description.id')
                ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
                ->select([
                    'products.id',
                    'product_description.name',
                    'products.model',
                    'products.location',
                    'products.price',
                    'products.updated_at',

                    \DB::raw('manufacturers.name as mname'),
                ]);

            $datatables = Datatables::of($products)
                ->addColumn('details_url', function ($product)
                {
                    return '<a href="' . url('/details/' . $product->id) . '"><i class="fa fa-plus-circle"></i></a>';
                });

         /*   if($model = $request->get('search')['value'])
            {
                $datatables->where('products.model', 'like', "$model%"); // additional users.name search
            }
*/

            return $datatables->make(true);
        }

        $html = $this->htmlBuilder
            ->addColumn(['data' => 'details_url', 'name' => 'details_url', 'title' => 'Detalii', 'className' => 'details-control', 'searchable' => false, 'orderable' => false, 'defaultContent' => '', 'width' => '5%'])
            ->addColumn(['data' => 'name', 'name' => 'product_description.name', 'title' => 'Produs', 'orderable' => false, 'width' => '30%'])
            ->addColumn(['data' => 'mname', 'name' => 'manufacturers.name', 'title' => 'Producator', 'orderable' => false, 'width' => '10%'])
            ->addColumn(['data' => 'model', 'name' => 'products.model', 'title' => 'Cod Produs', 'width' => '20%'])
            ->addColumn(['data' => 'location', 'name' => 'products.location', 'title' => 'Locatie', 'width' => '15%'])
            ->addColumn(['data' => 'price', 'name' => 'products.price', 'title' => 'Pret final', 'width' => '10%'])
            ->addColumn(['data' => 'updated_at', 'name' => 'products.updated_at', 'title' => 'Data calcularii', 'width' => '20%'])
            ->parameters(['processing' => 'true',
                          'serverside' => 'true',
                          'language'   => ['url' => 'js/romanian.json'],
                          'scrollY'    => 370,
                          "lengthMenu" => [[50, 100, 500, 1000], [50, 100, 500, 1000]],
                          "order"      => [[5, 'DESC']],
            ]);

        return view("pages.prices", compact('html'));
    }

    public function details($id)
    {

        $settings = \App\Settings::lists('value', 'key');

        $tva = 1 + $settings["tva"] / 100;

        $distribs = Cache::rememberForever('distribs', function() {
            return \App\Distrib::lists('name', 'id');
        });

        $product = \App\Product::find($id);

        $existing = $product->manufacturer->distribs()->lists('id')->all();

        return view("pages.details", compact("tva", "product", "distribs", "existing"));
    }

    public function store(ProductDistRequest $request)
    {
        $product = \App\Product::find($request->product_id);

        if($request->mode == "add") {
            $product->distribs()->attach($request->distrib_id);
        }

        if($request->mode == "delete") {
            $product->distribs()->detach($request->distrib_id);
        }

        $product->save();

    }

}
