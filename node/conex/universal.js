var site = "http://shop.conexdist.ro/2128-uleiuri-motoare",
    user = "andrei.nancu@yahoo.com",
    password = '00287700';

var Nightmare = require('nightmare'),
    fs = require('fs'),
    random = require('random-js'),
    Sequelize = require('sequelize'),
    equiv = require(__dirname + '/equiv/manufacturers'),
    vo = require('vo'),
    cheerio = require('cheerio'),
    mkdirp = require('mkdirp'),
    nconf = require('nconf'),
    foreach = require('generator-foreach'),
    path = require('path'),
    mv = require('mv');

nconf.argv()
    .env()
    .file({file: __dirname + '/config.json'});

var db = nconf.get('database'),
    scraper = nconf.get('scraper'),
    connection = new Sequelize(db.name, db.user, db.password, {
        host: db.host,
        dialect: db.dialect,
        logging: false
    }),
    models = require('sequelize-import')(__dirname + '/mysql_models', connection),
    product_queue = [];

function* run() {
    var nightmare = Nightmare({
        width: 1280,
        height: 1024,
    });


    yield nightmare
        .goto(site)
        .wait();
    
    var is_not_logged = yield nightmare
        .evaluate(function () {
            return $('input[name="SubmitLogin"]').length;
        });


    if (is_not_logged == 1) {
        yield nightmare
            .type('#email', user)
            .wait()
            .type('#passwd', password)
            .wait()
            .click('input[name="SubmitLogin"]')
            .wait();

        yield nightmare
            .goto(site)
            .wait();

    }
    var date = new Date();

    yield nightmare
        .evaluate(function () {
            var count = 0;
            var current = 0;
            function status() {
                var s = function () {
                    window.scrollTo(0, document.body.scrollHeight);
                    count++;
                };
                s();

                $(document).ajaxComplete(s);

                var i = setInterval(function () {
                    if (current == count) {
                        clearInterval(i);
                        $('body').append('<div class="scrolling-done"><h1>Done!!!!</h1></div>');
                    }
                    current = count;
                }, 5000);
                return true;
            }
           return status();
        });

    yield nightmare
       .wait('.scrolling-done');

    yield nightmare
        .screenshot(__dirname + '/test.png');


    var content = yield nightmare
        .evaluate(function () {
            return document.querySelector("body").innerHTML;
        });


 proc(content);

yield nightmare.end();
}


function proc(data) {

    nconf.argv()
        .env()
        .file({file: __dirname + '/universal.json'});

    $ = cheerio.load(data);

    if ($('.product-row').length) {
        $('.product-row').each(function (k, v) {

            var price = $(v).find('.price').text().replace(" lei", "").replace(' ', '').replace(',', '.').trim();
            var code = $(v).find('.product-list-cod-producator strong').text();
            var stock = $(v).find('.product-list-quantity strong').text().replace(/>/g, '').trim();
            var man = $(v).find('.product-list-brand strong').text();
            var name = $(v).find('.product-list-name a').text();
            var img = $(v).find('.fancybox.thickbox.shown').attr("href");

            nconf.set(code, {
                price: price,
                stock: stock,
                manufacturer: man,
                name: name,
                image: img
            });

        });

        nconf.save(function (err) {
            fs.readFile(__dirname + '/universal.json', function (err, data) {
                console.dir(JSON.parse(data.toString()))
            });
        });
    }
}


vo(run)(function (err, result) {
    if (err) throw err;
});