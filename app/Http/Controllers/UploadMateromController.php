<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessMaterom;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\MateromRequest;

class UploadMateromController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.materom');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(MateromRequest $request)
    {
        $request->file('file')->move(
            storage_path('app/xls'), 'materom.xlsx'
        );

        $this->dispatch(new ProcessMaterom());

        return redirect('materom')->with('status', 'Fisier incarcat cu success');
    }
}
