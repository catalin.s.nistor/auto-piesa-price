<div id="modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" >
                Cod Bardi
                </h4>
            </div>
            <div class="modal-body">
                <!-- Cod bardi Form Input For Cod bardi -->

                <div class="code_img" style="text-align: center">
                    <img src="{{asset('images/security_code.png')}}"/>
                </div>

                <div class="form-group">
                    {!! Form::label('Cod bardi', 'cod:') !!}
                    {!! Form::text('Cod bardi', null, ['class' => 'code form-control']) !!}
                </div>


            </div>
            <div class="alert bardi alert-danger" role="alert">Eroare, codul este invalid</div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Inchide</button>
                <button type="button" class="btn btn-success send">Trimite</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->