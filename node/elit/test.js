
var fs = require('fs'),
    random = require('random-js'),
    Sequelize = require('sequelize'),
    equiv = require(__dirname + '/equiv/manufacturers'),
    nconf = require('nconf');


nconf.argv()
    .env()
    .file({file: __dirname + '/config.json'});

var db = nconf.get('database');

var scraper = nconf.get('scraper'),
    connection = new Sequelize(db.name, db.user, db.password, {
        host: db.host,
        dialect: db.dialect,
        logging: false
    }),
    models = require('sequelize-import')(__dirname + '/mysql_models', connection),
    product_queue = [];

models.Product.hasOne(models.Elit, {foreignKey: 'product_id'});
models.Elit.belongsTo(models.Product, {foreignKey: 'id'});

equiv.manufacturers.forEach(function (manufacturer) {
    models.Product.findAll({
        order: [
            Sequelize.fn('RAND')
        ],
        limit: scraper.limit,
        where: {manufacturer_id: manufacturer.id},
        include: [
            {
                model: models.Elit,
                attributes: ['product_id'],
                paranoid: false,
                where: {product_id: null},
                required: false
            }
        ]
    }).then(function (products) {
        products.forEach(function (product) {
            var seconds = random.integer(1000, 2000)(random.engines.nativeMath);

            var obj = {
                id: product.id,
                model: product.model,
                manufacturer: manufacturer.name,
                manufacturer_id: manufacturer.id,
                sku: product.model,
                seconds: seconds
            };

            product_queue.push(obj);
        });
    });
});
