<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BardiPrice extends Model
{

    public $timestamps = false;

    protected $fillable = ['product_id', 'model', 'manufacturer_id'];

    public function product() {
        $this->belongsTo('App\Product');
    }


    public function getPriceAttribute($value)
    {
        return $this->tofloat(str_replace('.', '', $value));
    }

    private function tofloat($num) {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        }

        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
        );
    }
}
