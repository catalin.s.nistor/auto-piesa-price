var site = "http://ic-ro.intercars.eu",
    nclient = "R18606",
    ncard = "28606",
    password = 'R21IANANCU';

var file = '/tmp/intercars.lock';

var Nightmare = require('nightmare'),
    fs = require('fs'),
    random = require('random-js'),
    Sequelize = require('sequelize'),
    equiv = require(__dirname + '/equiv/manufacturers'),
    vo = require('vo'),
    cheerio = require('cheerio'),
    mkdirp = require('mkdirp'),
    nconf = require('nconf'),
    foreach = require('generator-foreach'),
    path = require('path'),
    mv = require('mv');

if(fs.existsSync(file))
    process.exit(1);

fs.closeSync(fs.openSync(file, 'w'));


nconf.argv()
    .env()
    .file({file: __dirname + '/config.json'});

var db = nconf.get('database'),
    scraper = nconf.get('scraper'),
    connection = new Sequelize(db.name, db.user, db.password, {
        host: db.host,
        dialect: db.dialect,
        logging: false
    }),
    models = require('sequelize-import')(__dirname + '/mysql_models', connection),
    product_queue = [];

models.Product.hasOne(models.Intercars, {foreignKey: 'product_id'});
models.Intercars.belongsTo(models.Product, {foreignKey: 'id'});

equiv.manufacturers.forEach(function (manufacturer) {
    models.Product.findAll({
        order: [
            Sequelize.fn('RAND')
        ],
        limit: scraper.limit,
        where: {manufacturer_id: manufacturer.id},
        include: [
            {
                model: models.Intercars,
                attributes: ['product_id'],
                paranoid: false,
                where: {product_id: null},
                required: false
            }
        ]
    }).then(function (products) {
        products.forEach(function (product) {
            var seconds = random.integer(3000, 10000)(random.engines.nativeMath);

            var obj = {
                id: product.id,
                model: product.model,
                manufacturer: manufacturer.name,
                manufacturer_id: manufacturer.id,
                sku: product.model,
                seconds: seconds
            };

            product_queue.push(obj);

        });
    });
});


function* run() {
    var nightmare = Nightmare({
        width: 1280,
        height: 1024,
    });

    yield nightmare
        .goto(site)
        .wait()
        .type('#kh_kod', nclient)
        .wait()
        .type('#kh_ksk', ncard)
        .wait()
        .type('#kh_has', password)
        .wait()
        .click('a.btn-confirm')
        .wait('#vKodP');


    yield * foreach(product_queue, function* (p, k) {

            var val = p.sku;

            yield nightmare
                .evaluate(function (val) {
                    jQuery('#vKodP').val(val);
                    jQuery('.btn-search').trigger('click');
                    return true;
                }, val);

            yield nightmare.wait(2000);

            var next_page = true;

            while (next_page) {


                var details_count = yield nightmare
                    .evaluate(function () {
                        if (jQuery('.btn-more').length)
                            return jQuery('.btn-more').length;
                        else
                            return false;
                    });

                if (details_count) {
                    for (var i = 1; i <= details_count; i++) {
                        yield nightmare
                            .evaluate(function (i) {
                                jQuery('.product-baner:nth-of-type(' + i + ') .btn-more').trigger('click');
                                return true;
                            }, i);

                        var content = yield nightmare
                            .wait(p.seconds)
                            .evaluate(function () {
                                return document.querySelector("#facebox").innerHTML;
                            });

                        proc(p.id, p.sku, p.manufacturer, p.manufacturer_id, content);

                        yield nightmare
                            .evaluate(function () {
                                jQuery('#facebox .close').trigger('click');
                                return true;
                            });

                        yield nightmare.wait(500);

                    }

                    next_page = yield nightmare
                        .evaluate(function () {
                            if (jQuery('.pagination a:nth-of-type(2)').hasClass('btn-global')) {
                                jQuery('.pagination a:nth-of-type(2)').trigger('click');
                                return true;
                            }
                            else
                                return false;
                        });


                    yield nightmare
                        .wait(1500);
                }

                else {
                    yield nightmare
                        .goto(site);
                    yield nightmare
                        .wait(2000);

                    next_page = false;

                    proc(p.id, p.sku, p.manufacturer, p.manufacturer_id, '');
                }
            }

        }
    );

    if(fs.existsSync(file))
        fs.unlink(file);

    yield nightmare.end();
}

function addProduct(intercars_code, product_id, sku, manufacturer_id, price, stock, manufacturer) {

    models.Intercars.findOrCreate({
        where: {
            intercars_code: intercars_code
        },
        defaults: {
            product_id: product_id,
            intercars_code: intercars_code,
            model: sku,
            manufacturer_id: manufacturer_id,
            manufacturer: manufacturer,
            price: price,
            stock: stock
        }
    });
}

function proc(product_id, sku, manufacturer, manufacturer_id, data) {

    if (data != '') {
        $ = cheerio.load(data);

        var price = $('.popup').find('.product-baner-price .bigFontGreen:nth-of-type(1)').text().replace(',', '.');
        var code = $('.popup').find('.karta-produktu-title .karta-produktu-indeks').text().replace("Index:", "").trim();
        var s1 = $('.popup').find('#dosoffline_content1-T .att-right').text().replace(/>/g, '').replace("bucati", "").trim();
        var s2 = $('.popup').find('#dosoffline_content2-T .att-right').text().replace(/>/g, '').replace("bucati", "").trim();
        var stock = parseInt(s1) + parseInt(s2);
        var man = $('.popup').find('.karta-produktu-title h3 b:nth-child(3)').text();

        addProduct(code, 0, "", 0, price, stock, man);
    }

    else
        addProduct(sku, product_id, sku, manufacturer_id, 0, 0, manufacturer);


}

vo(run)(function (err, result) {
    if (err) throw err;
});