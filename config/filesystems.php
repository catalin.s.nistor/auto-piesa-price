<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. A "local" driver, as well as a variety of cloud
    | based drivers are available for your choosing. Just store away!
    |
    | Supported: "local", "ftp", "s3", "rackspace"
    |
    */

    'default' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    */

    'disks' => [

        'root' => [
            'driver' => 'local',
            'root'   => storage_path(),
        ],

        'tmp' => [
            'driver' => 'local',
            'root'   => '/tmp/',
        ],

        'local' => [
            'driver' => 'local',
            'root'   => storage_path('app'),
        ],

        'node' => [
            'driver' => 'local',
            'root'   => base_path('/node/'),
        ],

        'public' => [
            'driver' => 'local',
            'root'   => base_path('/public/uploads/files/'),
        ],

        'ftp' => [
            'driver'   => 'ftp',
            'host'     => 'ftp.auto-piesa.ro',
            'username' => 'prod@auto-piesa.ro',
            'password' => 'l0ibnuiyg1q1',

            // Optional FTP Settings...
            // 'port'     => 21,
            // 'root'     => '',
            // 'passive'  => true,
            // 'ssl'      => true,
            // 'timeout'  => 30,
        ],

        'remotedb' => [
            'driver'   => 'ftp',
            'host'     => 'ftp.auto-piesa.ro',
            'username' => 'sync@auto-piesa.ro',
            'password' => '^s8]e@!+Uyy?',
        ],

        's3' => [
            'driver' => 's3',
            'key'    => 'your-key',
            'secret' => 'your-secret',
            'region' => 'your-region',
            'bucket' => 'your-bucket',
        ],

        'rackspace' => [
            'driver'    => 'rackspace',
            'username'  => 'your-username',
            'key'       => 'your-key',
            'container' => 'your-container',
            'endpoint'  => 'https://identity.api.rackspacecloud.com/v2.0/',
            'region'    => 'IAD',
            'url_type'  => 'publicURL',
        ],

    ],

];
