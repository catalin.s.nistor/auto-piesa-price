<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ResetDiscover extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:discover';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \App\ConexPrice::chunk(100, function ($p)
        {
            foreach ($p as $item)
            {
                $item->ran_discover_task = 0;
                $item->save();
            }
        });


        \App\BardiPrice::chunk(100, function ($p)
        {
            foreach ($p as $item)
            {
                $item->ran_discover_task = 0;
                $item->save();
            }
        });

        \App\ElitPrice::chunk(100, function ($p)
        {
            foreach ($p as $item)
            {
                $item->ran_discover_task = 0;
                $item->save();
            }
        });

        \App\IntercarsPrice::chunk(100, function ($p)
        {
            foreach ($p as $item)
            {
                $item->ran_discover_task = 0;
                $item->save();
            }
        });
    }
}
