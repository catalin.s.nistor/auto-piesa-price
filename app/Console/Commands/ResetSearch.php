<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ResetSearch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset:search';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \App\ConexPrice::chunk(100, function ($p)
        {
            foreach ($p as $item)
            {
                $item->updated = 0;
                $item->save();
            }
        });


        \App\ElitPrice::chunk(100, function ($p)
        {
            foreach ($p as $item)
            {
                $item->updated = 0;
                $item->save();
            }
        });
    }
}
