<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemoteProductDescription extends Model
{

    protected $connection = "sqlite";


    public $timestamps = false;
    
    public $table = 'product_description';

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * get product
     *
     */
    public function product() {
        return $this->belongsTo('App\RemoteProduct', 'id');
    }
}
