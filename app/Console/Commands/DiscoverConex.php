<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DiscoverConex extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discover:conex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Conex distributor product discovery.';

    /**
     * Equiv
     *
     * @var array
     */
    protected $equiv = [
        'DACIA' => 'DACIA Original',
        'ORIG RENAULT' => 'RENAULT Original'
    ];

    protected $replace_chars = [' ', '-', '.'];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function handle()
    {
        \App\ConexPrice::where('ran_discover_task', 0)
            ->chunk(500, function ($conex)
            {
                foreach ($conex as $p)
                    if($p->manufacturer != "")
                    {

                        $p->ran_discover_task = 1;
                        $p->save();

                        $manufacturer = $p->manufacturer;

                        if(array_key_exists($p->manufacturer, $this->equiv))
                            $manufacturer = $this->equiv[$p->manufacturer];

                        if($p->conex_code != '')
                        {
                            $product = $this->getProductId(str_replace($this->replace_chars, '', strtoupper($p->conex_code)), true, $manufacturer);
                            if($product)
                            {
                                $this->setProductId($product, $p);
                                continue;
                            }
                        }

                    }
            });
    }


    private function setProductId($product, $conex)
    {
        $a = strpos($conex->conex_code, strtoupper($product->model));
        $b = strpos(str_replace($this->replace_chars, '', $conex->conex_code), str_replace($this->replace_chars, '', strtoupper($product->model)));

        if($a != false || $b != false || $conex->elit_code == $product->model || strtoupper(strtoupper(str_replace($this->replace_chars, '', $conex->conex_code)) == strtoupper(str_replace($this->replace_chars, '', $product->model))))
        {
            $conex->product_id = $product->id;
            $conex->model = $product->model;
            $conex->manufacturer_id = $product->manufacturer_id;
            $conex->save();

            if($conex->stock > 0)
                \App\ProductHasPrice::firstOrCreate(['product_id' => $product->id]);
        }
    }

    private function getProductId($code, $trim, $manufacturer = "")
    {
        if(trim($code) == "" || strlen($code) == 0)
            return false;

        $count = \App\Product::direct($code, $trim)
            ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->where('manufacturers.name', $manufacturer)
            ->count();

        if($count == 1)
        {
            $product = \App\Product::direct($code, $trim)
                ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
                ->where('manufacturers.name', $manufacturer)
                ->first();

            return $product;
        }
    }

}
