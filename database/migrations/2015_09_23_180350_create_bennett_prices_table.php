<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBennettPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bennett_prices', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('model');
            $table->string('bennett_code');
            $table->string('manufacturer');
            $table->string('price');
            $table->integer('stock');

            $table->index('product_id');
            $table->index('manufacturer');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bennett_prices');
    }
}
