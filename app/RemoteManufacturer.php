<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemoteManufacturer extends Model
{

    protected $connection = "sqlite";

    public $table = "manufacturers";


    public $timestamps = false;

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * get all products
     *
     */

    public function products() {
        return $this->hasMany('App\RemoteProduct');
    }

}
