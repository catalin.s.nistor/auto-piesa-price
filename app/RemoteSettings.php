<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemoteSettings extends Model
{
    public $timestamps = false;

    protected $connection = "sqlite";

    public $table = "settings";

    protected $fillable = ['key', 'value'];

}
