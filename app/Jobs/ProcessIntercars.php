<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use League\Csv\Reader;
use Artisan;
use Storage;
use DB;

class ProcessIntercars extends Job implements SelfHandling, ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */


    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->attempts() < 2)
        {
            \App\IntercarsPrice::truncate();
            
            if(Storage::disk('local')->exists('csv/intercars.csv'))
            {

                $csv = Reader::createFromPath(storage_path('app/csv/intercars.csv'));

                foreach ($csv->fetch() as $row)
                {

                    $intercars = new \App\IntercarsPrice();
                    $intercars->intercars_code = $row[0];
                    $intercars->manufacturer = $row[1];
                    $intercars->price = $row[6];
                    $intercars->stock = intval($row[2]) + intval($row[3]) + intval($row[4]) + intval($row[5]);
                    $intercars->save();

                }
            }
            
            Artisan::call('discover:intercars');
            
        }
    }
}
