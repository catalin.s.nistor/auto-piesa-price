module.exports = function (sequelize, DataType) {
    return sequelize.define("DistP", {
            product_id:  {type: DataType.INTEGER, primary_key: true, field: 'product_id'},
            distrib_id: DataType.INTEGER
        },
        {
            timestamps: false,
            tableName: 'distrib_product',
            freezeTableName: true
        }
    );
}