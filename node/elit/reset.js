var
    fs = require('fs'),
    nconf = require('nconf'),
    path = require('path');


nconf.argv()
    .env()
    .file({file: __dirname + '/config.json'});

nconf.set('task:run', 0);

nconf.save(function (err) {
    fs.readFile(__dirname + '/config.json', function (err, data) {
        console.dir(JSON.parse(data.toString()))
    });
});