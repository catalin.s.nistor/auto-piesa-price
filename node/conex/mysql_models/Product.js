module.exports = function (sequelize, DataType) {
    return sequelize.define("Product", {
            id: {type: DataType.INTEGER, primaryKey: true},
            model: DataType.STRING,
            manufacturer_id: {type: DataType.INTEGER},
        },
        {
            timestamps: true,
            createdAt: 'created_at',
            updatedAt: 'updated_at',
            tableName: 'products',
            freezeTableName: true
        }
    );
}
