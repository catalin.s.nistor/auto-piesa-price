module.exports = function (sequelize, DataType) {
    return sequelize.define("Manufacturer", {
            id: {type: DataType.INTEGER, primaryKey: true},
            name: DataType.STRING,
        },
        {
            timestamps: false,
            tableName: 'manufacturers',
            freezeTableName: true
        }
    );
};