<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntercarsPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intercars_prices', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('model');
            $table->string('intercars_code');
            $table->string('manufacturer');
            $table->string('price');
            $table->integer('stock');
            $table->integer("ran_discover_task")->default(0);

            $table->integer('manufacturer_id');
            $table->index('product_id');
            $table->index('intercars_code');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('intercars_prices');

    }
}
