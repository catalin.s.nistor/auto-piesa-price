@extends('app')

@section('content')

    <h3>Procente adaosuri</h3>

    <div class="row">

        <div class="col-md-6">

            {!! Form::open(['action' => 'SettingsController@store', 'method' => 'post']) !!}

           <!-- l50 Form Input For less50 -->

            <div class="form-group @if($errors->get('l50')) {{ " has-error" }} @endif">
                {!! Form::label('l50', '0-50 Ron:', ['class' => 'control-label']) !!}
                {!! Form::text('l50', $settings['l50'], ['class' => 'form-control']) !!}
            </div>

            <!-- b50_100 Form Input For b50_100 -->

            <div class="form-group @if($errors->get('b50_100')) {{ " has-error" }} @endif">
                {!! Form::label('b50_100', '50-100 Ron:', ['class' => 'control-label']) !!}
                {!! Form::text('b50_100', $settings['b50_100'], ['class' => 'form-control']) !!}
            </div>

            <!-- b100_200 Form Input For b100_200 -->

            <div class="form-group @if($errors->get('b100_200')) {{ " has-error" }} @endif">
                {!! Form::label('b100_200', '100-200 Ron:', ['class' => 'control-label']) !!}
                {!! Form::text('b100_200', $settings['b100_200'], ['class' => 'form-control']) !!}
            </div>

            <!-- b200-500 Form Input For b200-500 -->

            <div class="form-group @if($errors->get('b200_500')) {{ " has-error" }} @endif">
                {!! Form::label('b200_500', '200-500 Ron:', ['class' => 'control-label']) !!}
                {!! Form::text('b200_500', $settings['b200_500'], ['class' => 'form-control']) !!}
            </div>

            <!-- g500 Form Input For g500 -->

            <div class="form-group @if($errors->get('g500')) {{ " has-error" }} @endif">
                {!! Form::label('g500', '> 500 Ron:', ['class' => 'control-label']) !!}
                {!! Form::text('g500', $settings['g500'], ['class' => 'form-control']) !!}
            </div>


            <!-- ulei Form Input For ulei -->

            <div class="form-group @if($errors->get('ulei')) {{ " has-error" }} @endif">
                {!! Form::label('ulei', 'Uleiuri:', ['class' => 'control-label']) !!}
                {!! Form::text('ulei', $settings['ulei'], ['class' => 'form-control']) !!}
            </div>

            <!-- baterii Form Input For baterii -->

            <div class="form-group @if($errors->get('baterii')) {{ " has-error" }} @endif">
                {!! Form::label('baterii', 'Baterii:', ['class' => 'control-label']) !!}
                {!! Form::text('baterii', $settings['baterii'], ['class' => 'form-control']) !!}
            </div>

            <!-- baterii Form Input For baterii -->

            <div class="form-group @if($errors->get('becuri')) {{ " has-error" }} @endif">
                {!! Form::label('becuri', 'Becuri:', ['class' => 'control-label']) !!}
                {!! Form::text('becuri', $settings['becuri'], ['class' => 'form-control']) !!}
            </div>

            <!-- lichide Form Input For lichide -->

            <div class="form-group @if($errors->get('lichide')) {{ " has-error" }} @endif">
                {!! Form::label('lichide', 'Lichide de intretinere:', ['class' => 'control-label']) !!}
                {!! Form::text('lichide', $settings['lichide'], ['class' => 'form-control']) !!}
            </div>

            <!-- tva Form Input For tva -->

            <div class="form-group">
                {!! Form::label('tva', 'Procent TVA:') !!}
                {!! Form::text('tva', $settings['tva'], ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Salveaza', ['class' => 'btn btn-primary form-control']) !!}
            </div>


            {!! Form::close()  !!}


    @if($errors->any())

        <ul class="alert alert-danger">
            <li>Verificati campurile (valoarea campurilor trebuie sa fie cuprinsa intre 0 si 100)</li>
        </ul>

    @endif
        </div>

    </div>
@endsection