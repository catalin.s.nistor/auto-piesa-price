@extends('app')


@section('content')
    <h2>Lista de preturi</h2>

    {!! $html->table() !!}
@endsection


@section('footer')

    {!! $html->scripts() !!}

    <script type="text/javascript">
        $(document).ready(function () {

            var table = window.LaravelDataTables["dataTableBuilder"];

            $(".table.dataTable").on("click", "td.details-control ", function (e) {
                e.preventDefault();
                var tr = $(this).closest('tr');
                var row = table.row(tr);

                var link = $(this).find("a").attr("href");

                if (row.child.isShown()) {
                    // This row is already open - close it
                    row.child.hide();

                    $(tr).find("i").addClass("fa-plus-circle");
                    $(tr).find("i").removeClass("fa-minus-circle");

                    tr.removeClass('shown');
                }
                else {

                    $.get(link, function (data) {
                        row.child(data).show();
                        $(tr).find("i").removeClass("fa-plus-circle");
                        $(tr).find("i").addClass("fa-minus-circle");
                        tr.addClass('shown');
                    });
                }
            });
        });

    </script>

@endsection