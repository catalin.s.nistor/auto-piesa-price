<?php

namespace App\Http\Controllers;

use App\Http\Requests\IntercarsRequest;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessIntercars;
use App\Jobs\GenerateIntercars;
use Illuminate\Http\Request;
use \League\Csv\Reader;
use App\Http\Requests;
use Storage;
use SSH;

class UploadIntercarsController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.intercars');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(IntercarsRequest $request)
    {

        if(Storage::disk('local')->has('csv/intercars.csv'))
             Storage::disk('local')->delete('csv/intercars.csv');

        $request->file('file')->move(
            storage_path('app/csv'), 'intercars.csv'
        );

        $this->dispatch(new ProcessIntercars());

       return redirect('intercars')->with('status', 'Fisier incarcat cu success');
    }

    public function download(Request $request)
    {
        if($request)
        {
            if(Storage::disk('public')->has('intercars.xml'))
            {
                Storage::disk('public')->delete('intercars.xml');
            }

            $this->dispatch(new GenerateIntercars());

        }

    }

    public function status(Request $request)
    {
        if($request)
        {
            if(Storage::disk('public')->has('intercars.xml'))
            {
                return url("/uploads/files/intercars.xml");
            }
        }
    }

    public function vm_start(Request $request)
    {

        if($request->start)
        {
            SSH::into('local')->run([
                'sh ~/vm-spice.sh'
            ]);

            echo "started";
        }
    }

    public function vm_stop(Request $request)
    {
        if($request->stop)
        {
            SSH::into('local')->run([
                'rm -f /tmp/vm-spice.lock',
                'killall qemu-system-x86_64 &> /dev/null'

            ]);

            echo "stop";
        }
    }

    public function vm_restart(Request $request)
    {
        if($request->restart)
        {

            SSH::into('local')->run([
                'sh ~/vm-spice-restart.sh',
            ]);
        }
    }

    public function vm_status(Request $request)
    {
        if($request->status)
        {

            $flag = true;

            $commands = [
                'ps axf | grep "qemu-system-x86_64" | grep -v grep'
            ];

            SSH::into('local')->run($commands, function ($line) use (&$flag)
            {
                if($line != "")
                    $flag = false;
            });

            if($flag)
            {
                SSH::into('local')->run([
                    'rm /tmp/vm-spice.lock'
                ]);
            }

            else {
                SSH::into('local')->run([
                    'touch /tmp/vm-spice.lock'
                ]);
            }

            if(Storage::disk('tmp')->has('vm-spice.lock'))
                echo "1";
            else
                echo "0";
        }
    }

}
