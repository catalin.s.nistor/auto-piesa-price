<script>

    $(document).ready(function () {
        $(".alert.bardi").hide();

        var modal_enable = false;

        $('.open-modal').hide();


        var ws;

        start();

        function start() {
            try {
                 ws = new WebSocket('ws://{{ str_replace(['http://', 'https://'], '', url('/')) }}:8080', ['soap', 'xmpp']);

                ws.onerror = function (error) {
                    console.log('WebSocket Error ' + error);
                };

                ws.onmessage = function (e) {
                    if(e.data == "code") {
                        modal_enable = true;
                        $('.open-modal').show();

                        $.playSound("{{asset('sound/notification')}}");
                        $(".code_img img").attr('src', $(".code_img img").attr('src')+'?'+Math.random());
                        if(!$('.modal').is(':visible'))
                            $('.modal').modal('show');
                    }
                };

                ws.onclose = function(e)  {
                    console.log('closed');
                    setTimeout(start, 5000);
                }
            }

            catch(err) {
                ws.close();
                console.log("retry");
            }
        }




        $(".send").click(function(){
            var code = $(".code").val();

            if(code.length > 0) {
                modal_enable = false;
                $('.open-modal').hide();

                $(".alert.bardi").hide();
                ws.send(code);
                $('.modal').modal('hide');
                $(".code").val("");
            }
            else
                $(".alert.bardi").show();
        });

        $('.code').keypress(function(e) {
            if(e.which == 13) {
                modal_enable = false;
                $('.open-modal').hide();

                var code = $(".code").val();
                if(code.length > 0) {
                    $(".alert.bardi").hide();
                    ws.send(code);
                    $('.modal').modal('hide');
                    $(".code").val("");
                }
                else
                    $(".alert.bardi").show();
            }
        });

        $('.open-modal').click(function (e) {
            e.preventDefault();
            if(modal_enable)
                $('.modal').modal('show');
        });

        });

</script>

