<?php


namespace App\Console\Commands;

ini_set("error_reporting", E_ALL & ~E_DEPRECATED);

use Illuminate\Console\Command;
use anlutro\cURL\cURL;
use League\Csv\Reader;
use DB;

class DiscoverBennett extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discover:bennett';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bannet distributor product discovery.';

    /**
     * Equiv
     *
     * @var array
     */

    protected $equiv = [
        //bennet //auto-piesa
        'FEBI BILSTEIN'  => 'FEBI',
        'MULLER FILTER'  => 'MULLER',
        'MAHLE ORIGINAL' => 'MAHLE',
        'Saleri SIL'     => 'SIL',
        'GKN'            => 'GKN  (SPIDAN)',
        'LuK'            => 'LUK',
        'ERA S.p.A.'     => 'ERA',
        'MULLER FILTER'  => 'MULLER'
    ];

    /**
     * Curl
     *
     * @var
     */

    protected $curl;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(cURL $cURL)
    {
        parent::__construct();

        $this->curl = $cURL;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    protected $manufacturers_to_attach = [];
    

    public function handle()
    {

        //echo "GETTING FILE \n";

        $extClient = new \nusoap_client("http://info3.bennett-auto.ro/server.php", false, false, false, false, false, 3000, 3000);
        $param = array("93d37d4369dd69fb5ff0fb10b174c468");
        $extResult = $extClient->call("listaCoduriDetalii", array($param));
        
        //echo "GOT FILE \n";

        
        if(isset($extResult['file']) && filter_var($extResult['file'], FILTER_VALIDATE_URL))
        {

            DB::table('distrib_manufacturer')->where('distrib_id', 2)->delete();

            \App\BennettPrice::truncate();
            
            $url = $this->curl->buildUrl($extResult['file'], []);
            $response = $this->curl->get($url);

            $csv = Reader::createFromString($response);
            $data = $csv->query();

            foreach ($data as $lineIndex => $row)
            {
                //do something here
                $bennett = \App\BennettPrice::firstOrNew(
                    [
                        'model'        => $row[1],
                        'bennett_code' => $row[0],
                        'manufacturer' => $row[3],
                    ]
                );

                $bennett->model = $row[1];
                $bennett->bennett_code = $row[0];
                $bennett->manufacturer = $row[3];
                $bennett->price = $row[4];
                $bennett->stock = $row[5];
                $bennett->save();
            }
        }

        //echo "PROCESS";

        \App\BennettPrice::chunk(500, function ($bennett)
        {
            foreach ($bennett as $p)
                if($p->manufacturer != "")
                {

                    $manufacturer = $p->manufacturer;

                    if(array_key_exists($p->manufacturer, $this->equiv))
                        $manufacturer = $this->equiv[$p->manufacturer];

                    if($p->bennett_code != '')
                    {
                        $product = $this->getProductId($p->bennett_code, $manufacturer);

                        if($this->endsWith($p->bennett_code, "CT") && $manufacturer == "CONTITECH")
                        {

                            $product = $this->getProductId(substr($p->bennett_code, 0, - 2), $manufacturer);

                        }

                        if($this->endsWith($p->bennett_code, "/M") && $manufacturer == "MULLER")
                        {
                            $product = $this->getProductId(substr($p->bennett_code, 0, - 2), $manufacturer);
                        }

                        if($product)
                        {
                            $this->setProductId($product, $p);
                            continue;
                        }
                    }
                }
        });
        
        \App\Manufacturer::chunk(500, function ($manufacturer) {
            foreach ($manufacturer as $m)
                if(in_array($m->id, $this->manufacturers_to_attach))
                {
                    $m->distribs()->attach(2);
                }
        });
    }


    private function setProductId($product, $bennett)
    {
        $bennett->product_id = $product->id;
        $bennett->save();
        
        if(!in_array($product->manufacturer->id, $this->manufacturers_to_attach)) {
            $this->manufacturers_to_attach[] = $product->manufacturer->id;
        }
        
        //$product->manufacturer->distribs()->sync([2], false);

        if($bennett->stock > 0)
            \App\ProductHasPrice::firstOrCreate(['product_id' => $product->id]);
    }

    private function getProductId($code, $manufacturer = "")
    {
        if(trim($code) == "" || strlen($code) == 0)
            return false;

        $count = \App\Product::direct($code)
            ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->where('manufacturers.name', $manufacturer)
            ->count();

        if($count == 1)
        {
            $product = \App\Product::direct($code)
                ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
                ->where('manufacturers.name', $manufacturer)
                ->first();

            return $product;
        }
        /*
                $count = \App\Product::direct($code)
                    ->count();

                if($count == 1)
                {
                    $product = \App\Product::direct($code)
                        ->first();

                    return $product;
                }

                $count = \App\Product::reverseCode($code, $trim)
                    ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
                    ->where('manufacturers.name', $manufacturer)
                    ->count();

                if($count == 1)
                {
                    $product = \App\Product::reverseCode($code, $trim)
                        ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
                        ->where('manufacturers.name', $manufacturer)
                        ->first();

                    return $product;
                }

                if(!$repeat)
                    return false;

                if($repeat && $count != 1)
                {
                    $code = substr($code, 0,-1);

                    return $this->getProductId($code, $trim, $manufacturer, $repeat);
                }
        */
    }

    function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if($length == 0)
        {
            return true;
        }

        return (substr($haystack, - $length) === $needle);
    }

}
