<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Auto-Piesa</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/">Preturi</a></li>
                <li><a href="/settings">Setari</a></li>
                <li><a href="/materom">Materom</a></li>
                <li><a href="/intercars">Intercars</a></li>
                <li><a href="/manufacturers">Setari Distribuitori</a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://dash.auto-piesa.dynu.com">Monitor</a></li>
                <li><a href="/stats">Extra</a></li>
                <li><a class="open-modal" href="" data-target="#modal">Cod Bardi</a></li>
                <li><a href="/auth/logout">Iesire</a></li>

            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>