@extends('app')

@section('content')

    <h3>Upload Materom</h3>

    <div class="row">

        <div class="col-md-6">

            {!! Form::open([
                'action' => 'UploadMateromController@store',
                'files' => 'true'])
             !!}

            <div class="form-group">
                {!! Form::label('Tabel excel Materom') !!}
                {!! Form::file('file', null) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Salveaza', ['class' => 'btn btn-primary form-control']) !!}
            </div>


            {!! Form::close()  !!}


            @if($errors->any())

                <ul class="alert alert-danger">
                    <li>Incarcati un fisier de tip xlsx</li>
                </ul>

            @endif

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

        </div>

    </div>
@endsection