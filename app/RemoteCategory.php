<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemoteCategory extends Model
{
    protected $connection = "sqlite";

    public $table = "categories";

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     * get all products
     *
     */

    public function categories() {
        return $this->belongsToMany('App\RemoteCategory', 'category_product', 'product_id', 'category_id');
    }

}
