var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix.sass('app.scss');

    mix.styles([
        'bootstrap.min.css',
        'jquery.dataTables.min.css',
        'jquery.fileupload-noscript.css',
        'jquery.fileupload-ui-noscript.css',
        'jquery.fileupload-ui.css',
        'jquery.fileupload.css',
    ]);

    mix.scripts([
        'jquery.min.js',
        'bootstrap.min.js',
        'jquery.dataTables.min.js',
        'jquery.playSound.js',
        'jquery.WebSocket.js.min.js',
        'jquery.ui.widget.js',
        'jquery.iframe-transport.js',
        'jquery.fileupload.js',
    ]);

});
