var Shuffle = require('shuffle');

var manufacturers = [
    {id: 103, name: 'Ajusa'},
    {id: 32, name: 'Aral'},
    {id: 19, name: 'Asam'},
    {id: 44, name: 'Beru'},
    {id: 173, name: 'Calorstat'},
    {id: 93, name: 'Castrol'},
    {id: 174, name: 'Clean Filters'},
    {id: 92, name: 'Corteco'},
    {id: 8, name: 'Dacia Original'},
    {id: 24, name: 'Dolz'},
    {id: 131, name: 'Hepu'},
    {id: 36, name: 'Lemforder'},
    {id: 73, name: 'Liqui Modi'},
    {id: 190, name: 'Lobro'},
    {id: 25, name: 'Luk'},
    {id: 209, name: 'Macht'},
    {id: 212, name: 'Mannol'},
    {id: 70, name: 'Mobil'},
    {id: 63, name: 'Remsa'},
    {id: 12, name: 'Renault Original'},
    {id: 57, name: 'SNR'},
    {id: 210, name: 'Tess Conex'},
    {id: 211, name: 'Thermix'},
    {id: 214, name: 'Total'},
    {id: 49, name: 'Valeo'}
];

module.exports = {
    manufacturers: Shuffle.shuffle({deck: manufacturers}).cards
};