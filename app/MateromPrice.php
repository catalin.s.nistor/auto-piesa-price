<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MateromPrice extends Model
{
    //public $timestamps = false;

    protected $guarded = [];

    public function product() {
        $this->belongsTo('App\Product');
    }
}
