<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

class Product extends Model {

    /**
     * Fillable
     *
     * @var array
     */
    protected $fillable = ['model', 'stock', 'price', 'location', 'manufacturer_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * get manufacturers
     *
     */
    public function manufacturer()
    {
        return $this->belongsTo('App\Manufacturer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * get categories
     *
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category', 'category_product', 'product_id', 'category_id');
    }

    public function distribs()
    {
        return $this->belongsToMany('App\Distrib', 'distrib_product', 'product_id', 'distrib_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * get product description
     *
     */
    public function description()
    {
        return $this->hasOne('App\ProductDescription', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * get bardi price
     *
     */
    public function bardi()
    {
        return $this->hasOne('App\BardiPrice');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * get elit price
     */
    public function elit()
    {
        return $this->hasOne('App\ElitPrice');
    }

    public function elit_old()
    {
        return $this->hasOne('App\ElitPriceOld');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * get bennet price
     */
    public function bennett()
    {
        return $this->hasOne('App\BennettPrice');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * get conex price
     */
    public function conex()
    {
        return $this->hasOne('App\ConexPrice');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * get intercars price
     */
    public function intercars()
    {
        return $this->hasOne('App\IntercarsPrice');
    }

    public function materom()
    {
        return $this->hasOne('App\MateromPrice');
    }

    /**
     * @param $query
     * @param $code
     * @param bool|true $trim
     * @return mixed
     *
     * code %search%
     */
    public function scopeCode($query, $code, $trim = true)
    {
        if($trim)
            return $query->select(["products.id", "products.manufacturer_id", "products.model"])->where('products.s_model', 'LIKE', "%$code");

        return $query->select(["products.id", "products.manufacturer_id", "products.model"])->where('products.model', 'LIKE', "%$code%");

    }

    /**
     * @param $query
     * @param $code
     * @param bool|true $trim
     * @return mixed
     *
     * code search%
     */
    public function scopeReverseCode($query, $code, $trim = true)
    {
        if($trim)
            return $query->select(["products.id", "products.manufacturer_id", "products.model"])->where('products.s_model', 'LIKE', "$code%");

        return $query->select(["products.id", "products.manufacturer_id", "products.model"])->where('products.model', 'LIKE', "$code%");

    }

    /**
     * @param $query
     * @param $code
     * @param bool|true $trim
     * @return mixed
     *
     * code search
     */
    public function scopeDirect($query, $code, $trim = true)
    {
        $code = str_replace(["'", '.', '-', '_', '/'], '', $code);

        if($trim)
            return $query->select(["products.id", "products.manufacturer_id", "products.model"])->where('products.s_model', 'LIKE', "$code");

        return $query->select(["products.id", "products.manufacturer_id", "products.model"])->where('products.model', 'LIKE', "$code");

    }

    public function scopeDirectUpper($query, $code, $trim = true)
    {
        $code = str_replace(["'"], '', $code);

        if($trim)
            return $query->select(["products.id", "products.manufacturer_id", "products.model"])->where('products.s_model', 'LIKE', "$code");

        return $query->select(["products.id", "products.manufacturer_id", "products.model"])->where('products.model', 'LIKE', "$code");

    }

}
