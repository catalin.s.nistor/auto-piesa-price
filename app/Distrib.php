<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distrib extends Model
{
    protected $touches = ['product'];

    public function products() {
        return $this->belongsToMany('App\Product');
    }


    public function manufacturers() {
        return $this->belongsToMany('App\Manufacturer');
    }
}
