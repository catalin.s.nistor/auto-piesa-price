/**
 * Created by cata on 03/09/15.
 */
module.exports = function (sequelize, DataType) {
    return sequelize.define("Bardi", {
            id: {type: DataType.INTEGER, primaryKey: true, autoIncrement: true, unique: true},
            product_id: {type: DataType.INTEGER},
            model: DataType.STRING,
            bardi_code: DataType.STRING,
            price: DataType.STRING,
            stock: DataType.INTEGER,
            manufacturer: {type: DataType.STRING},
            manufacturer_id: {type: DataType.INTEGER},
            createdAt: {
                field: 'created_at',
                type: DataType.DATE
            },
            updatedAt: {
                field: 'updated_at',
                type: DataType.DATE
            }

        },
        {
            timestamps: true,
            tableName: 'bardi_prices',
            freezeTableName: true
        }
    );
}
