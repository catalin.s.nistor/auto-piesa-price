<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DiscoverElit extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discover:elit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Elit distributor product disvovery.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    protected $ate;

    protected $equiv = [
        'ELIT'             => 'MASTER SPORT',
        'MSP'              => 'MASTER SPORT',
        'K&N'              => ' K & N',
        'MANN'             => 'MANN-FILTER',
        'BOSCH ROBERT'     => 'BOSCH',
        'NISS'             => 'NISSENS',
        'VERNET'           => 'CALORSTAT by Vernet',
        'VERN'             => 'CALORSTAT by Vernet',
        'NIP'              => 'NIPPARTS',
        'ALKO PERFORMANCE' => 'AL- KO',
        'ALKO'             => 'AL- KO',
        'KYB PERFORMANC'   => 'KYV',
        'AE'               => 'AE (FEDERAL MOGUL)'
    ];


    protected $replace_chars = [' ', '-', '.'];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->ate = include(storage_path('app/ate_equiv_elit.php'));
        $ate = [];

        foreach ($this->ate as $k => $v)
        {
            $ate['ATE ' . $k] = $v;
        }

        \App\ElitPrice::where('ran_discover_task', 0)->chunk(500, function ($elit) use ($ate)
        {
            foreach ($elit as $p)

            if($p->manufacturer != "")
            {

                if($p->stock != 0 || $p->stock != 1)

                    $cond = [];
                    $cond[] = is_numeric(strpos($p->stock, 'Informaţii la cerere')) ? 1 : 0;
                    $cond[] = is_numeric(strpos($p->stock, 'Verificare disponibilitate'))? 1 : 0;
                    $cond[] = is_numeric(strpos($p->stock, 'Indisponibil momentan'))? 1 : 0;

                if(in_array(1, $cond))
                        $p->stock = 0;
                    else
                        $p->stock = 1;


                $p->ran_discover_task = 1;
                $p->save();

                $manufacturer = $p->manufacturer;

                if(array_key_exists($p->manufacturer, $this->equiv))
                {
                    $manufacturer = $this->equiv[$p->manufacturer];
                }

                $match = true;

                $elit_code = $p->elit_code;

                if($manufacturer == 'MASTER SPORT')
                    $elit_code = str_replace('-PCS', '', $p->elit_code) . '-PCS-MS';

                if($manufacturer == "ATE" && isset($ate[$p->elit_code]))
                {
                    $elit_code = $ate[$p->elit_code];
                    $match = false;
                }

                $product = $this->getProductId(str_replace($this->replace_chars, '', strtoupper($elit_code)), true, $manufacturer);

                if($product)
                {
                    $this->setProductId($product, $p, $match);
                    continue;
                }
            }
        });
    }

    private function setProductId($product, $elit, $match)
    {
        if($match)
        {
            $a = strpos($elit->elit_code, strtoupper($product->model));
            $b = strpos(str_replace([' ', '-', '.'], '', $elit->elit_code), str_replace($this->replace_chars, '', strtoupper($product->model)));

            if($a != false || $b != false || $elit->elit_code == $product->model || strtoupper(strtoupper(str_replace($this->replace_chars, '', $elit->elit_code)) == strtoupper(str_replace($this->replace_chars, '', $product->model))))
            {
                $elit->product_id = $product->id;
                $elit->model = $product->model;
                $elit->manufacturer_id = $product->manufacturer_id;
                $elit->save();

                if($elit->stock > 0)
                    \App\ProductHasPrice::firstOrCreate(['product_id' => $product->id]);
            }

        } else
        {
            $elit->product_id = $product->id;
            $elit->model = $product->model;
            $elit->manufacturer_id = $product->manufacturer_id;
            $elit->save();

            if($elit->stock > 0)
                \App\ProductHasPrice::firstOrCreate(['product_id' => $product->id]);
        }
    }

    private function getProductId($code, $trim = true, $manufacturer = "")
    {

        if(trim($code) == "" || strlen($code) == 0)
            return false;

        $count = \App\Product::direct($code, $trim)
            ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->where('manufacturers.name', $manufacturer)
            ->count();


        if($count == 1)
        {
            $product = \App\Product::direct($code, $trim)
                ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
                ->where('manufacturers.name', $manufacturer)
                ->first();

            return $product;
        }

        $count = \App\Product::directUpper($code, $trim)
            ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->where('manufacturers.name', $manufacturer)
            ->count();

        if($count == 1)
        {
            $product = \App\Product::directUpper($code, $trim)
                ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
                ->where('manufacturers.name', $manufacturer)
                ->first();

            return $product;
        }

        if($count != 1)
        {
            $code = substr($code, 1);
            return $this->getProductId($code, $trim, $manufacturer);
        }


    }
}
