<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public $table = "categories";

    public $timestamps = false;

    protected $guarded = ['price'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     *
     * get all products
     *
     */

    public function categories() {
        return $this->belongsToMany('App\Product');
    }

}
