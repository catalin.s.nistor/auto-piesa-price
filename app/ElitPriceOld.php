<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElitPriceOld extends Model
{
    public $timestamps = false;

    public $table = 'elit_prices_old';

    protected $fillable = ['product_id', 'model', 'manufacturer_id'];

    public function product() {
        $this->belongsTo('App\Product');
    }

}
