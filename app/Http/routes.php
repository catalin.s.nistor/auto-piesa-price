<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');



Route::match(['get', 'post'], '/', "PricesController@index");
Route::match(['get', 'post'], '/details/{id}', "PricesController@details");

Route::get('settings', 'SettingsController@index');
Route::post('settings/store', 'SettingsController@store');

Route::get('materom', 'UploadMateromController@index');
Route::match(['get', 'post'], 'materom/store', 'UploadMateromController@store');

Route::get('intercars', 'UploadIntercarsController@index');
Route::post('intercars/store', 'UploadIntercarsController@store');
Route::post('intercars/download', 'UploadIntercarsController@download');
Route::post('intercars/status', 'UploadIntercarsController@status');
Route::post('intercars/vmstop', 'UploadIntercarsController@vm_stop');
Route::post('intercars/vmstart', 'UploadIntercarsController@vm_start');
Route::post('intercars/vmrestart', 'UploadIntercarsController@vm_restart');
Route::post('intercars/vmstatus', 'UploadIntercarsController@vm_status');


Route::get('stats', 'StatsController@index');
Route::post('stats/store', 'StatsController@store');

Route::get('manufacturers', 'ManufacturersController@index');
Route::post('manufacturers/store', 'ManufacturersController@store');

Route::post('prices/store', 'PricesController@store');
