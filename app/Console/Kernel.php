<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\SetPrices::class,
        \App\Console\Commands\DiscoverBardi::class,
        \App\Console\Commands\DiscoverElit::class,
        \App\Console\Commands\DiscoverBennett::class,
        \App\Console\Commands\DiscoverConex::class,
        \App\Console\Commands\DiscoverIntercars::class,
        \App\Console\Commands\DiscoverMaterom::class,
        \App\Console\Commands\NewProducts::class,
        \App\Console\Commands\UploadPrices::class,
        \App\Console\Commands\UploadConexUniversal::class,
        \App\Console\Commands\Sync::class,
        \App\Console\Commands\ResetDiscover::class,
        \App\Console\Commands\ResetSearch::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('queue:work')->everyMinute();

        $schedule->exec(base_path() . '/node/bardi/run_bardi.sh')
            ->everyMinute();

        $schedule->exec(base_path() . '/node/conex/run_conex.sh')
            ->everyMinute();

        $schedule->exec(base_path() . '/node/conex/reset.sh')
            ->dailyAt('20:00');

        $schedule->exec(base_path() . '/node/elit/reset.sh')
            ->dailyAt('20:00');

        $schedule->exec(base_path() . '/node/elit/run_elit.sh')
            ->everyMinute();

        $schedule->exec(base_path() . '/node/conex/run_universal.sh')
            ->weekly();

        $schedule->exec(base_path() . '/node/kill.sh')
            ->hourly();

        $schedule->command('discover:elit')
            ->everyFiveMinutes()
            ->withoutOverlapping();


        $schedule->command('discover:bardi')
            ->everyFiveMinutes()
            ->withoutOverlapping();

        $schedule->command('discover:conex')
            ->everyFiveMinutes()
            ->withoutOverlapping();

        $schedule->command('reset:search')
            ->monthly();

        $schedule->command('discover:bennett')
            ->dailyAt('21:00');

        $schedule->command('sync:all')
            ->twiceDaily(10, 20);

    }
}
