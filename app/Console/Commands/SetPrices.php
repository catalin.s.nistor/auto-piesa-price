<?php

namespace App\Console\Commands;

use App\Product;
use Illuminate\Console\Command;
use Artisan;

class SetPrices extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set:prices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    
    public function handle()
    {
        Artisan::call('discover:products');

        $settings = \App\Settings::lists('value', 'key');

        \App\Product::chunk(100, function ($p) {

            foreach ($p as $product)
            {
                $product->price = 0;
                $product->location = '';
                $product->quantity = 0;
                $product->save();
            }

        });
        
        \App\ProductHasPrice::chunk(100, function ($pp) use (&$settings)
        {
            foreach ($pp as $p)
            {
                $prices = [];

                $bardi = \App\BardiPrice::where('product_id', $p->product_id)->first();

                if($bardi && is_numeric($bardi->price))
                    $prices['bardi'] = $bardi->price;

                /*
                  $elit_old = \App\ElitPriceOld::where('product_id', $p->product_id)->first();

                  if($elit_old && is_numeric($elit_old->price) && $elit_old->price > 0 && $elit_old->stock > 0)
                     $prices['elit'] = $elit_old->price * (1+ $settings['tva'] / 100);
                */

                $elit = \App\ElitPrice::where('product_id', $p->product_id)->first();

                if($elit && is_numeric($elit->price) && $elit->price > 0  && $elit->stock > 0)
                    $prices['elit'] = $elit->price * (1 + $settings['tva'] / 100);

                $bennett = \App\BennettPrice::where('product_id', $p->product_id)->first();

                if($bennett && is_numeric($bennett->price) && $bennett->price > 0  && $bennett->stock > 0)
                    $prices['bennett'] = $bennett->price * (1 + $settings['tva'] / 100);

                $conex = \App\ConexPrice::where('product_id', $p->product_id)->first();

                if($conex && is_numeric($conex->price) && $conex->price > 0 && $conex->stock > 0)
                    $prices['conex'] = $conex->price * (1 + $settings['tva'] / 100);

                $intercars = \App\IntercarsPrice::where('product_id', $p->product_id)->first();

                if($intercars && is_numeric($intercars->price) && $intercars->price > 0 && $intercars->stock > 0)
                    $prices['intercars'] = $intercars->price;

                $materom = \App\MateromPrice::where('product_id', $p->product_id)->first();

                if($materom && is_numeric($materom->price) && $materom->price > 0 && $materom->stock > 0)
                    $prices['materom'] = $materom->price * (1 + $settings['tva'] / 100);

                if(empty($prices))
                    continue;

                $location = array_search(min($prices), $prices);

                if($location)
                {
                    $price = $prices[$location];

                    $add = 1;

                    if($price < 50)
                        $add = ($settings['l50'] / 100) + 1;

                    if($price >= 50 && $price <= 100)
                        $add = ($settings['b50_100'] / 100) + 1;

                    if($price >= 100 && $price <= 200)
                        $add = ($settings['b100_200'] / 100) + 1;

                    if($price >= 200 && $price <= 500)
                        $add = ($settings['b200_500'] / 100) + 1;

                    if($price > 500)
                        $add = ($settings['g500'] / 100) + 1;


                    $product = \App\Product::join('category_product', 'category_product.product_id', '=', 'products.id')
                        ->where('products.id', $p->product_id)
                        ->whereNotIn('category_product.category_id', [169, 165, 166, 168])
                        ->first();

                    if($product)
                    {
                        
                        $price = ceil($price * $add);
                        
                        if($product->price != $price)
                        {
                            
                            $product->price = $price;
                            $product->location = strtoupper($location);
                            $product->quantity = 100;
                            $product->save();
                        }
                    }

                    if(!$product)
                    {
                        $product = \App\Product::join('category_product', 'category_product.product_id', '=', 'products.id')
                            ->where('products.id', $p->product_id)
                            ->whereIn('category_product.category_id', [169, 165, 166, 168])
                            ->first();

                        if($product)
                        {
                            if(in_array($product->category_id, [165, 166])) //uleiuri
                                $add = ($settings['ulei'] / 100) + 1;

                            if($product->category_id == 168)
                                $add = ($settings['baterii'] / 100) + 1;

                            if($product->category_id == 169)
                                $add = ($settings['becuri'] / 100) + 1;
                             
                            $price = ceil($price * $add);
                            
                            if($product->price != $price)
                            {
                                $product->price = $price;
                                $product->location = strtoupper($location);
                                $product->quantity = 100;
                                $product->save();
                            }
                        }
                    }
                }
            }
        });
    }
}