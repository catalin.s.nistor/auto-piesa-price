var site = "http://shop.conexdist.ro",
    user = "office@auto-piesa.ro",
    password = '00287700',
    did = 3,
    total_runs = 80;

var file = '/tmp/conex.lock';

var Nightmare = require('nightmare'),
    fs = require('fs'),
    random = require('random-js'),
    Sequelize = require('sequelize'),
    equiv = require(__dirname + '/equiv/manufacturers'),
    vo = require('vo'),
    cheerio = require('cheerio'),
    mkdirp = require('mkdirp'),
    nconf = require('nconf'),
    foreach = require('generator-foreach'),
    path = require('path'),
    mv = require('mv');

if (fs.existsSync(file))
    process.exit(1);

fs.closeSync(fs.openSync(file, 'w'));

nconf.argv()
    .env()
    .file({file: __dirname + '/config.json'});

var db = nconf.get('database'),
    scraper = nconf.get('scraper'),
    connection = new Sequelize(db.name, db.user, db.password, {
        host: db.host,
        dialect: db.dialect,
        logging: true
    }),
    models = require('sequelize-import')(__dirname + '/mysql_models', connection),
    product_queue = [];


var times_run = nconf.get('task:run');

if (times_run > total_runs) {
    if (fs.existsSync(file)) {
        fs.unlink(file);
    }
    process.exit(1);
}

models.Product.hasMany(models.DistP, {foreignKey: 'product_id', constraints: false});

models.Product.findAll({
    include: [{
        model: models.DistP,
        attributes: [],
        where: {distrib_id: did},
    }],
    order: Sequelize.literal('DistPs.created_at DESC')
}).then(function (products) {
    products.forEach(function (product) {

        models.Conex.findOne({where: {product_id: product.id}}).then(function (conex) {
            if (conex == null) {

                var seconds = random.integer(5000, 20000)(random.engines.nativeMath);

                var obj = {
                    id: product.id,
                    model: product.model,
                    manufacturer_id: product.manufacturer_id,
                    sku: product.model,
                    seconds: seconds
                };

                product_queue.push(obj);
            }
        });
    });
});


models.DistM.findAll({attributes: ['manufacturer_id'], where: {distrib_id: did}}).then(function (distms) {
    var m = [];
    distms.forEach(function (distm) {
        equiv.manufacturers.forEach(function (ms) {
            if (ms.id == distm.manufacturer_id)
                m.push(ms);
        });
    });
    return m;

}).done(function (manufacturers) {
    manufacturers.forEach(function (manufacturer) {
        models.Product.findAll({
            order: [
                Sequelize.fn('RAND')
            ],
            limit: scraper.limit,
            where: {manufacturer_id: manufacturer.id}
        }).then(function (products) {
            products.forEach(function (product) {
                models.Conex.findOne({where: {product_id: product.id}}).then(function (conex) {

                    if (conex == null) {
                        var seconds = random.integer(5000, 20000)(random.engines.nativeMath);

                        var obj = {
                            id: product.id,
                            model: product.model,
                            manufacturer_id: product.manufacturer_id,
                            sku: product.model,
                            seconds: seconds
                        };
                        product_queue.push(obj);
                    }
                });
            });
        });

    });
});


function* run() {

    var nightmare = Nightmare({
        width: 1280,
        height: 1024,
    });

    yield nightmare
        .goto(site)
        .wait();

    var is_not_logged = yield nightmare
        .evaluate(function () {
            return $('input[name="SubmitLogin"]').length;
        });


    if (is_not_logged == 1) {
        yield nightmare
            .type('#email', user)
            .wait()
            .type('#passwd', password)
            .wait()
            .click('input[name="SubmitLogin"]')
            .wait();
       
        yield nightmare.screenshot(__dirname + '/login.png');
    }
    


    yield * foreach(product_queue, function*(p, k) {

        times_run++;

        nconf.set('task:run', times_run);

        nconf.save(function (err) {
            fs.readFile(__dirname + '/config.json', function (err, data) {
                console.dir(JSON.parse(data.toString()))
            });
        });

        yield nightmare
            .evaluate(function () {
                $('input[name="search_query"]').attr("value", "");
            });

        yield nightmare
            .type('input[name="search_query"]', p.sku)
            .click('button[type="submit"]');

        yield nightmare.wait();

        var alert = yield nightmare
            .evaluate(function () {
                return $('.alert.alert-danger').length;
            });

        if (alert == 0) {
            yield nightmare
                .evaluate(function () {
                    var count = 0;
                    var current = 0;

                    function status() {
                        var s = function () {
                            window.scrollTo(0, document.body.scrollHeight);
                            count++;
                        };
                        s();

                        $(document).ajaxComplete(s);

                        var i = setInterval(function () {
                            if (current == count) {
                                clearInterval(i);
                                $('body').append('<div class="scrolling-done"><h1>Done!!!!</h1></div>');
                            }
                            current = count;
                        }, 5000);
                        return true;
                    }

                    return status();
                });

            yield nightmare
                .wait('.scrolling-done');
        }

        var content = yield nightmare
            .wait(p.seconds)
            .evaluate(function () {
                return document.querySelector("body").innerHTML;
            });

        proc(p.id, p.sku, p.manufacturer_id, content);

        yield nightmare
            .screenshot(__dirname + '/current.png');

        if (times_run > total_runs) {
            if (fs.existsSync(file)) {
                fs.unlink(file);
            }

            process.exit(1);
        }


    });

    if (fs.existsSync(file))
        fs.unlink(file);


    yield nightmare.end();

}

function addProduct(conex_code, price, stock, manufacturer) {


    models.Conex.findOrCreate({
        where: {
            conex_code: conex_code
        },
        defaults: {
            conex_code: conex_code,
            manufacturer: manufacturer,
            price: price,
            stock: stock,
            updated: 1
        }
    }).spread(function (product) {
        product.update({
            price: price,
            stock: stock,
            updated: 1
        }, {fields: ['price', 'stock', 'updated']});
    });

}

function proc(product_id, sku, manufacturer_id, data) {

    $ = cheerio.load(data);

    if ($('.product-row').length) {
        $('.product-row').each(function (k, v) {

            var price = $(v).find('.product-list-price .price').text().replace(" lei", "").replace(' ', '').replace(',', '.').trim();
            var code = $(v).find('.product-list-cod-producator strong').text();
            var stock = $(v).find('.product-list-quantity strong').text().replace(/>/g, '').trim();
            var man = $(v).find('.product-list-brand strong').text();

            addProduct(code, price, stock, man);
        });
    }

}

vo(run)(function (err, result) {
    if (err) throw err;
});
