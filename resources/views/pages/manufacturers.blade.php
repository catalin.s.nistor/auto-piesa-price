@extends('app')

@section('content')

    <h3>Setari producatori</h3>

    <div class="row">
        <div class="col-md-12">

            {!! Form::open(['action' => 'ManufacturersController@store', 'method' => 'post']) !!}

            <table class="table">
                <tr>
                    <th>Producatori</th>
                    <th>Distribuitori</th>
                </tr>
                @foreach($manufacturers as $man_id => $man)

                    <tr>
                        <td>{{$man}}</td>
                        <td style="font-size: 12px">
                            @foreach($distribs as $distrib_id => $distrib)
                                {!! Form::checkbox('settings['. $man_id . '][]', $distrib_id, \App\Manufacturer::find($man_id)->distribs()->find($distrib_id) ? true : false) !!} {{$distrib}}
                            @endforeach
                        </td>
                    </tr>

                @endforeach
            </table>

            <div class="col-md-4 form-group">
                {!! Form::submit('Salveaza', ['class' => 'btn btn-primary form-control']) !!}
            </div>

            <div class="col-md-4 form-group">
               <button class="btn btn-primary form-control reset">Reset</button>
            </div>

        </div>
        {!! Form::close() !!}
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.reset').click(function(e){
                e.preventDefault();
                $('input:checkbox').removeAttr('checked');
            });
        });
    </script>
@endsection