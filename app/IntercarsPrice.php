<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IntercarsPrice extends Model
{
    //public $timestamps = false;

    protected $guarded = [];

    public function product() {
        $this->belongsTo('App\Product');
    }

}
