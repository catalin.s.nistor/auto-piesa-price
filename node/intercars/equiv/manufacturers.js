var Shuffle = require('shuffle');

var manufacturers = [
    //{id: 19, name: 'ASAM'},
    {id: 30, name: 'BOSCH'},
    /*  {id: 173, name: 'CALORSTAT by Vernet'},
     {id: 21, name: 'CONTITECH'},
     {id: 8, name: 'DACIA Original'},
     {id: 91, name: 'DENSO'},
     {id: 84, name: 'GATES'},
     {id: 118, name: 'GYROTEC'},
     {id: 131, name: 'HEPU'},
     {id: 119, name: 'INA'},
     {id: 29, name: 'KYB'},
     {id: 115, name: 'MEYLE'},
     {id: 128, name: 'NGK'},
     {id: 23, name: 'OPTIMAL'},
     {id: 66, name: 'PHILIPS'},
     {id: 12, name: 'RENAULT Original'},
     {id: 117, name: 'SCT Germany'},
     {id: 211, name: 'THERMIX'},
     {id: 210, name: 'TESS CONEX'},*/
];

module.exports = {
    manufacturers: Shuffle.shuffle({deck: manufacturers}).cards
};