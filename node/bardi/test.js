var fs = require('fs'),
    cheerio = require('cheerio');



var data = fs.readFileSync(__dirname + '/scrapes/ZS052.txt');
var sku = 'ZS052';


$ = cheerio.load(data);

try {
    if ($('.search-result-list-content').length) {
        $('.search-result-list-content').each(function (k, v) {
            var bardi_code = $(v).data('pn');
            var manufacturer = $(v).data('gyarto');
            var stock_flag = 0;



            if (bardi_code.search(sku.replace(/\s+/g, '')) != -1) {
                var price = $(v).find('.search-result-list-purchase-price-td-content').text().replace("lei", "").trim();
                if (!(~price.search("-")))
                    console.log("\n\n" +bardi_code + ' | ' + price + ' | ' + manufacturer + "\n");
            }
            else {
                var price = $(v).find('.search-result-list-purchase-price-td-content').text().replace("lei", "").trim();
                if (!(~price.search("-")))
                    console.log("\n\n" + bardi_code + ' | ' + price + ' | ' + manufacturer + "\n");
            }

            $(v).find(".search-result-list-retail-amount-td img").each(function (k, v) {
                if (!($(v).attr("title") == "Ploieşti Nu există la filială." || $(v).attr("title") == "Nu există la Centru."))
                    stock_flag = 1;
                console.log($(v).attr("title"));
            });

            console.log(stock_flag + "\n");

        });
    }

    else if ($('#product_sheet_main_data').length) {
        var price = $("#product_sheet_base_data_container .green").text().replace("lei", "").trim();
        var bc = $("#product_sheet_base_data_container > h2 > span").text();
        var bardi_code = bc.substring(1, bc.length - 1);
        var manufacturer = $("#product_sheet_base_data_container > h2").text().replace(bc, '').trim();

        var stock_flag = 0;

        $(".product_sheet_newamount img").each(function (k, v) {

            if (!($(v).attr("title") == "Ploieşti Nu există la filială" || $(v).attr("title") == "Nu există la Centru.")) {
                stock_flag = 1;
            }
        });
        if (bardi_code.search(sku.replace(/\s+/g, '')) != -1)
            console.log(bardi_code + ' | ' + price + ' | ' +  stock_flag + ' | ' + manufacturer);
        else
            console.log(bardi_code + ' | ' + price + ' | ' +  stock_flag + ' | ' + manufacturer);
    }

    else if ($('.block').length) {

    }
}

catch (err) {
    console.log(err);
}