module.exports = function (sequelize, DataType) {
    return sequelize.define("DistM", {
            manufacturer_id:  DataType.INTEGER,
            distrib_id: DataType.INTEGER
        },
        {
            timestamps: false,
            tableName: 'distrib_manufacturer',
            freezeTableName: true
        }
    );
}