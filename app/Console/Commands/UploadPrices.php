<?php

namespace App\Console\Commands;

use Storage;
use Illuminate\Console\Command;
use Mockery\CountValidator\Exception;

class UploadPrices extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'upload:prices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload prices to the main site.';

    /**
     * File
     *
     * @var string
     */
    protected $file = 'mysql/product_updates.sql';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        if(Storage::disk('local')->exists($this->file))
            Storage::disk('local')->delete($this->file);

        \App\Product::where('price', '!=', 0)->chunk(1000, function ($p)
        {
            foreach ($p as $product)
            {
                $query = "UPDATE oc_product SET price ='" . $product->price . "' location ='" . $product->location . "' WHERE product_id =" . $product->id . ';' ;
                    Storage::disk('local')->append($this->file, $query);
                }

        });
    }

}
