var cheerio = require('cheerio'),
Sequelize = require('sequelize');
var fs = require('fs');
var mkdirp = require('mkdirp');
var base = require('app-root-path');
var exit = require('exit');

var nconf = require('nconf');

nconf.argv()
    .env()
    .file({ file: base + '/config.json' });

var db = nconf.get('database');

var connection = new Sequelize(db.name, db.user, db.password, {
    host: db.host,
    dialect: db.dialect,
    logging: false
});

var models = require('sequelize-import')(base + '/mysql_models', connection);

function addProduct(bardi_code, product_id, sku, manufacturer_id, price, stock, manufacturer) {

    models.Bardi.findOrCreate({
        where: {
            bardi_code: bardi_code
        },
        defaults: {
            product_id: product_id,
            bardi_code: bardi_code,
            model: sku,
            manufacturer_id: manufacturer_id,
            manufacturer: manufacturer,
            price: price,
            stock: stock
        }
    });

}

var process = function (product_id, sku, manufacturer, manufacturer_id, data) {

    $ = cheerio.load(data);
    mkdirp(base + '/scrapes/' + manufacturer);

    fs.writeFile(base + '/scrapes/' + manufacturer + "/" + product_id + ".txt", data, function(err){
        if(err)
            console.log(err);
    });

    try {
        if ($('.search-result-list-content').length) {
            $('.search-result-list-content').each(function (k, v) {
                var bardi_code = $(v).data('pn');
                var manufacturer = $(v).data('gyarto');
                var stock_flag = 0;

                $(".search-result-list-retail-amount-td img").each(function (k, v) {
                    if (!($(v).attr("title") == "Ploieşti Nu există la filială" && $(v).attr("title") == "Nu există la Centru."))
                        stock_flag = 1;
                });

                if (bardi_code.search(sku.replace(/\s+/g, '')) != -1) {
                    var price = $(v).find('.search-result-list-purchase-price-td-content').text().replace("lei", "").trim();
                    if (!(~price.search("-")))
                        addProduct(bardi_code, product_id, sku, manufacturer_id, price, stock_flag, manufacturer);
                }
                else {
                    var price = $(v).find('.search-result-list-purchase-price-td-content').text().replace("lei", "").trim();
                    if (!(~price.search("-")))
                        addProduct(bardi_code, 0, "", manufacturer_id, price, stock_flag, manufacturer);
                }
            });
        }

        else if ($('#product_sheet_main_data').length) {
            var price = $("#product_sheet_base_data_container .green").text().replace("lei", "").trim();
            var bc = $("#product_sheet_base_data_container > h2 > span").text();
            var bardi_code = bc.substring(1, bc.length - 1);
            var manufacturer = $("#product_sheet_base_data_container > h2").text().replace(bc,'').trim();

            var stock_flag = 0;

            $(".product_sheet_newamount img").each(function (k, v) {

                if (!($(v).attr("title") == "Ploieşti Nu există la filială" && $(v).attr("title") == "Nu există la Centru.") && k < 2) {
                    stock_flag = 1;
                }
            });
            if (bardi_code.search(sku.replace(/\s+/g, '')) != -1)
                addProduct(bardi_code, product_id, sku, manufacturer_id, price, stock_flag, manufacturer);
            else
                addProduct(bardi_code, 0, null, manufacturer_id, price, stock_flag, manufacturer);
        }

        else if ($('.block').length) {
            nconf.set('code:needed', true);

            nconf.save(function (err) {
                fs.readFile(base + '/config.json', function (err, data) {
                    console.dir(JSON.parse(data.toString()))
                });
            });
        }
    }

    catch(err) {
        console.log(err);
    }
}

var exports = module.exports = {
    content: process
};
