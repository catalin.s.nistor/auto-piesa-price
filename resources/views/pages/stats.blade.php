@extends('app')

@section('content')

    <h3>Resetare data pentru produse noi adaugate</h3>

    <div class="row">

        <div class="col-md-4">

            {!! Form::open(['action' => 'StatsController@store', 'method' => 'post']) !!}

            <div class="form-group">
                {!! Form::submit('Reset', ['class' => 'btn btn-primary form-control']) !!}
            </div>


            {!! Form::close() !!}

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

        </div>
    </div>

@endsection