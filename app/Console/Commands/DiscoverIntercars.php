<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \App\IntercarsPrice;
use Storage;
use DB;


class DiscoverIntercars extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discover:intercars';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Intercars distributor product disvovery.';


    /**
     * Equiv
     *
     * @var array
     */
    protected $equiv = [
        'BOSCH Injection'      => 'BOSCH',
        'BOSCH Brakes'         => 'BOSCH',
        'BOSCH Wipers'         => 'BOSCH',
        'TRW AUTOMOTIVE'       => 'TRW',
        'MAGNETI MARELLI'      => 'MAGNETTI MARELLI',
        'AE'                   => 'AE (FEDERAL MOGUL)',
        'AJUSA'                => 'AJUSA',
        'ALKAR'                => 'ALKAR',
        'ATE - TEVES'          => 'ATE',
        'BERU'                 => 'BERU',
        'BOSAL'                => 'BOSAL',
        'BOSCH Belts'          => 'BOSCH',
        'BOSCH Brakes '        => 'BOSCH',
        'BOSCH Electrics'      => 'BOSCH',
        'BOSCH Filers '        => 'BOSCH',
        'BOSCH Filters '       => 'BOSCH',
        'CORTECO'              => 'CORTECO',
        'DAYCO'                => 'DAYCO',
        'DELPHI DIESEL'        => 'DELPHI',
        'DELPHI KLIMA'         => 'DELPHI',
        'DELPHI WTRYSK'        => 'DELPHI',
        'DENSO KLIMA'          => 'DENSO',
        'DENSO REMAN'          => 'DENSO',
        'DENSO WTRYSK'         => 'DENSO',
        'DOLZ'                 => 'DOLZ',
        'ELRING'               => 'ELRING',
        'ERA'                  => 'ERA',
        'EXEDY CORPORATION1'   => 'EXEDY',
        'FAE'                  => 'FAE',
        'FAG Bearings'         => 'FAG',
        'FAG Hydraulics'       => 'FAG',
        'FEBI BILSTEIN'        => 'FEBI',
        'GOETZE'               => 'GOETZE ENGINE',
        'HELLA'                => 'HELLA PAGID',
        'HELLA KLIMA'          => 'HELLA',
        'HERTH+BUSS JAKOPARTS' => 'JAKOPARTS',
        'HUTCHINSON'           => 'HUTCHINSON',
        'IMPERGOM'             => 'IMPERGOM',
        'KYB'                  => 'KYB',
        'K&N'                  => 'K & N',
        'LEMFOERDER'           => 'LEMFORDER',
        'LIQUI MOLY MOTO'      => 'LIQUI MOLY',
        'LPR'                  => 'LPR',
        'LUCAS'                => ['LUCAS DIESEL', 'LUCAS ELECTRICAL', 'LUCAS ENGINE DRIVE'],
        'LUCAS KLIMA'          => 'LUCAS ELECTRICAL',
        'LUK1'                 => 'LUK',
        'LUK2'                 => 'LUK',
        'MEAT & DORIA'         => 'MEAT & DORIA',
        'MERITOR'              => 'ARVIN MERITOR',
        'MONROE'               => 'MONROE',
        'NGK'                  => 'NGK',
        'NISSENS'              => 'NISSENS',
        'NRF'                  => 'NRF',
        'NRF KLIMA'            => 'NRF',
        'OE RENAULT'           => 'RENAULT Original',
        'OPTIMAL'              => 'OPTIMAL',
        'OSRAM'                => 'OSRAM',
        'PURFLUX'              => 'PURFLUX',
        'RUVILLE'              => 'RUVILLE',
        'SACHS1'               => 'SACHS',
        'SACHS2'               => 'SACHS',
        'SASIC'                => 'SASIC',
        'SPIDAN'               => 'GKN (SPIDAN)',
        'SWAG'                 => 'SWAG',
        'TEXTAR'               => 'TEXTAR',
        'TRW AUTOMOTIVE '      => 'TRW',
        'TYC'                  => 'TYC',
        'VAICO'                => 'VAICO',
        'VALEO'                => 'VALEO',
        'VALEO KLIMA'          => 'VALEO',
        'VALEO WYCIERACZKI'    => 'VALEO',
        'VALEO1'               => 'VALEO',
        'VALEO2'               => 'VALEO',
        'VDO REMAN'            => 'VDO',
        'VDO WTRYSK'           => 'VDO',
        'VERNET'               => 'CALORSTAT by Vernet',
        'WAHLER'               => 'WAHLER',
        'WALKER'               => 'WALKER',
        'ZF'                   => 'ZF PARTS',
    ];

    protected $manufacturers_to_attach = [];


    protected $replace_chars = [' ', '-', '.'];

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        DB::table('distrib_manufacturer')->where('distrib_id', 5)->delete();

        \App\IntercarsPrice::chunk(1000, function ($intercars)
        {
            foreach ($intercars as $p)
                if($p->manufacturer != "")
                {
                    $p->ran_discover_task = 1;
                    $p->save();

                    $manufacturer = $p->manufacturer;
                    $intercars_code = $p->intercars_code;

                    if($p->manufacturer == 'BERU')
                    {
                        $code = explode(' ', trim($intercars_code));
                        $intercars_code = $code[0];
                    }

                    if(array_key_exists($p->manufacturer, $this->equiv))
                    {
                        if(is_array($this->equiv[$p->manufacturer]))
                            foreach ($this->equiv[$p->manufacturer] as $m)
                            {
                                $manufacturer = $m;
                                $product = $this->getProductId(str_replace([' ', '-', '.'], '', strtoupper($intercars_code)), true, $manufacturer);
                            }
                        else
                        {
                            $manufacturer = $this->equiv[$p->manufacturer];
                            $product = $this->getProductId(str_replace([' ', '-', '.'], '', strtoupper($intercars_code)), true, $manufacturer);
                        }
                    } else
                    {
                        $product = $this->getProductId(str_replace([' ', '-', '.'], '', strtoupper($intercars_code)), true, $manufacturer);
                    }

                    if(is_object($product))
                    {
                        $this->setProductId($product, $p);
                        continue;
                    }
                }
        });

        \App\Manufacturer::chunk(500, function ($manufacturer)
        {
            foreach ($manufacturer as $m)
                if(in_array($m->id, $this->manufacturers_to_attach))
                {
                    $m->distribs()->attach(5);
                }
        });
    }

    private function setProductId($product, $intercars)
    {
        if(is_object($product))
        {
            $a = strpos($intercars->intercars_code, strtoupper($product->model));
            $b = strpos(str_replace([' ', '-', '.'], '', $intercars->intercars_code), str_replace($this->replace_chars, '', strtoupper($product->model)));

            if($a != false || $b != false || $intercars->intercars_code == $product->model || strtoupper(strtoupper(str_replace($this->replace_chars, '', $intercars->intercars_code)) == strtoupper(str_replace($this->replace_chars, '', $product->model))))
            {
                $intercars->product_id = $product->id;
                $intercars->model = $product->model;
                $intercars->manufacturer_id = $product->manufacturer_id;
                $intercars->save();

                if(!in_array($product->manufacturer->id, $this->manufacturers_to_attach))
                {
                    $this->manufacturers_to_attach[] = $product->manufacturer->id;
                }

                if($intercars->stock > 0)
                    \App\ProductHasPrice::firstOrCreate(['product_id' => $product->id]);
            }
        }
        
    }

    private function getProductId($code, $trim = true, $manufacturer = "")
    {
        if(trim($code) == "" || strlen($code) == 0)
            return false;

        $count = \App\Product::direct($code, $trim)
            ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->where('manufacturers.name', $manufacturer)
            ->count();

        if($count == 1)
        {
            $product = \App\Product::direct($code, $trim)
                ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
                ->where('manufacturers.name', $manufacturer)
                ->first();

            return $product;
        }

        $count = \App\Product::directUpper($code, $trim)
            ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->where('manufacturers.name', $manufacturer)
            ->count();

        if($count == 1)
        {
            $product = \App\Product::directUpper($code, $trim)
                ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
                ->where('manufacturers.name', $manufacturer)
                ->first();

            return $product;
        }

        if($count != 1)
        {
            $code = substr($code, 1);

            return $this->getProductId($code, $trim, $manufacturer);
        }


    }
}
