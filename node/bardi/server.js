var site = "http://www.bardiauto.ro/webshop",
    user = "office@auto-piesa.ro",
    password = 'bardi',
    site_search = "/gyari-szam-kereso/",
    did = 1;

var file = '/tmp/bardi.lock';

var Nightmare = require('nightmare'),
    fs = require('fs'),
    random = require('random-js'),
    Sequelize = require('sequelize'),
    equiv = require(__dirname + '/equiv/manufacturers'),
    vo = require('vo'),
    cheerio = require('cheerio'),
    mkdirp = require('mkdirp'),
    nconf = require('nconf'),
    foreach = require('generator-foreach'),
    path = require('path'),
    mv = require('mv');


nconf.argv()
    .env()
    .file({file: __dirname + '/config.json'});

if (fs.existsSync(file))
    process.exit(1);

fs.closeSync(fs.openSync(file, 'w'));

var db = nconf.get('database'),
    scraper = nconf.get('scraper'),
    connection = new Sequelize(db.name, db.user, db.password, {
        host: db.host,
        dialect: db.dialect
    }),
    models = require('sequelize-import')(__dirname + '/mysql_models', connection),
    product_queue = [];

models.Product.hasMany(models.DistP, {foreignKey: 'product_id', constraints: false});

equiv.manufacturers.forEach(function (manufacturer) {
    models.Product.findAll({
        where: {manufacturer_id: manufacturer.id},
        include: [{
            model: models.DistP,
            attributes: [],
            where: {distrib_id: 1},

        }],
        order: Sequelize.literal('DistPs.created_at DESC')

    }).then(function (products) {
        products.forEach(function (product) {
            models.Bardi.findOne({where: {product_id: product.id}}).then(function (bardi) {

                if (bardi == null) {

                    var seconds = random.integer(5000, 20000)(random.engines.nativeMath);

                    if (manufacturer.name != "SWAG")
                        var sku = product.model.replace(/\s+/g, '');
                    else
                        var sku = product.model.replace(/\s+/g, ':20');

                    var obj = {
                        id: product.id,
                        model: product.model,
                        manufacturer: manufacturer.name,
                        manufacturer_id: manufacturer.id,
                        bardi_url: manufacturer.bardi_url,
                        sku: sku,
                        seconds: seconds
                    };

                    product_queue.push(obj);
                }
            });
        });
    });
});

models.DistM.findAll({attributes: ['manufacturer_id'], where: {distrib_id: did}}).then(function (distms) {
    var m = [];
    distms.forEach(function (distm) {
        equiv.manufacturers.forEach(function (ms) {
            if (ms.id == distm.manufacturer_id)
                m.push(ms);
        });
    });
    return m;

}).done(function (manufacturers) {
    manufacturers.forEach(function (manufacturer) {
        models.Product.findAll({
            order: [
                Sequelize.fn('RAND')
            ],
            limit: scraper.limit,
            where: {manufacturer_id: manufacturer.id}
        }).then(function (products) {
            products.forEach(function (product) {
                models.Bardi.findOne({where: {product_id: product.id}}).then(function (bardi) {

                    if (bardi == null) {

                        var seconds = random.integer(5000, 20000)(random.engines.nativeMath);

                        if (manufacturer.name != "SWAG")
                            var sku = product.model.replace(/\s+/g, '');
                        else
                            var sku = product.model.replace(/\s+/g, ':20');

                        var obj = {
                            id: product.id,
                            model: product.model,
                            manufacturer: manufacturer.name,
                            manufacturer_id: manufacturer.id,
                            bardi_url: manufacturer.bardi_url,
                            sku: sku,
                            seconds: seconds
                        };

                        product_queue.push(obj);
                    }
                });
            });
        });
    });

});


var WebSocketServer = require('ws').Server,
    wss = new WebSocketServer({port: 8080}),
    CLIENTS = [];

var code = '';
var code_needed = false;
wss.on('connection', function (ws) {
    CLIENTS[ws._socket._handle.fd] = ws;
    ws.on('message', function (message) {
        code = message;
    });

    if (code_needed)
        ws.send("code");
});


function sendAll() {
    CLIENTS.forEach(function (c, k) {
        try {
            c.send("code");
        }
        catch (err) {
            delete CLIENTS[k];
        }
    });
}

function getCode() {
    sendAll();
    code = '';
    code_needed = true;
    return new Promise(function (resolve, reject) {
        setInterval(function () {
            if (code != '') {
                resolve(code);
            }
        }, 50);
    });
}

function* run() {


    var nightmare = Nightmare({
        width: 1280,
        height: 1024,
    });

    yield nightmare
        .goto(site)
        .wait()
        .click('li.pm_menu.padding')
        .wait()
        .type('.pm_login input[name="email"]', user)
        .type('.pm_login input[name="jelszo"]', password)
        .click('.pm_login .fake_btn')
        .wait()
        .wait();

    yield * foreach(product_queue, function* (p, k) {

        var date = new Date();

        var url = site + site_search + p.sku + "/" + p.bardi_url;

        console.log(date.toISOString() + ' ' + url);

        var status = yield nightmare
            .goto(url)
            .wait()
            .evaluate(function () {
                if ($('.block').length) {
                    return true;
                }
                return false;
            });

        if (status) {
            yield nightmare.wait(1000);

            yield nightmare
                .screenshot(__dirname + '/security_code.png', {
                    x: 550,
                    y: 120,
                    width: 150,
                    height: 150
                });

            try {
                mv('./security_code.png', path.resolve('../../public/images/security_code.png'), {mkdirp: true}, function () {
                });
            }

            catch (err) {
                console.log(err);
            }

            date = new Date();

            console.log(date.toISOString() + ' code needed');
            var c = yield getCode();
            code_needed = false;
            console.log(date.toISOString() + ' thank you!');


            yield nightmare
                .type('.inner-box input[name="captcha"]', c)
                .click('#captcha_submit.normal_button')
                .wait()
                .screenshot(__dirname + '/success.png');
        }

        var content = yield nightmare
            .wait(p.seconds)
            .evaluate(function () {
                return document.querySelector("body").innerHTML;
            });

        yield nightmare.screenshot(__dirname + '/text.png');

        proc(p.id, p.sku, p.manufacturer, p.manufacturer_id, content);

    });

    yield nightmare.end();
    wss.close();

    if (fs.existsSync(file))
        fs.unlink(file);

}


function addProduct(bardi_code, product_id, sku, manufacturer_id, price, stock, manufacturer) {

    models.Bardi.findOrCreate({
        where: {
            bardi_code: bardi_code
        },
        defaults: {
            product_id: product_id,
            bardi_code: bardi_code,
            model: sku,
            manufacturer_id: manufacturer_id,
            manufacturer: manufacturer,
            price: price,
            stock: stock
        }
    });

}

function proc(product_id, sku, manufacturer, manufacturer_id, data) {

    //  fs.writeFileSync(__dirname + '/scrapes/' + sku + '.txt', data);

    $ = cheerio.load(data);

    try {
        if ($('.search-result-list-content').length) {
            $('.search-result-list-content').each(function (k, v) {
                var bardi_code = $(v).data('pn');
                var manufacturer = $(v).data('gyarto');
                var stock_flag = 0;

                $(v).find(".search-result-list-retail-amount-td img").each(function (k, v) {
                    if (!($(v).attr("title") == "Ploieşti Nu există la filială." || $(v).attr("title") == "Nu există la Centru."))
                        stock_flag = 1;
                });

                if (bardi_code.search(sku.replace(/\s+/g, '')) != -1) {
                    var price = $(v).find('.search-result-list-purchase-price-td-content').text().replace("lei", "").trim();
                    if (!(~price.search("-")))
                        addProduct(bardi_code, product_id, sku, manufacturer_id, price, stock_flag, manufacturer);
                }
                else {
                    var price = $(v).find('.search-result-list-purchase-price-td-content').text().replace("lei", "").trim();
                    if (!(~price.search("-")))
                        addProduct(bardi_code, 0, "", manufacturer_id, price, stock_flag, manufacturer);
                }
            });
        }

        else if ($('#product_sheet_main_data').length) {
            var price = $("#product_sheet_base_data_container .green").text().replace("lei", "").trim();
            var bc = $("#product_sheet_base_data_container > h2 > span").text();
            var bardi_code = bc.substring(1, bc.length - 1);
            var manufacturer = $("#product_sheet_base_data_container > h2").text().replace(bc, '').trim();

            var stock_flag = 0;

            $(".product_sheet_newamount img").each(function (k, v) {
                if (!($(v).attr("title") == "Ploieşti Nu există la filială" || $(v).attr("title") == "Nu există la Centru.")) {
                    stock_flag = 1;
                }
            });
            if (bardi_code.search(sku.replace(/\s+/g, '')) != -1)
                addProduct(bardi_code, product_id, sku, manufacturer_id, price, stock_flag, manufacturer);
            else
                addProduct(bardi_code, 0, null, manufacturer_id, price, stock_flag, manufacturer);
        }

        else if ($('.block').length) {

        }
    }

    catch (err) {
        console.log(err);
    }
}

vo(run)(function (err, result) {
    if (err) throw err;
});
