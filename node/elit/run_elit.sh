#!/bin/sh

cd "$(dirname "$0")"
nohup xvfb-run -a --server-args="-screen 0 1280x1024x24" node server.js > /dev/null 2>&1