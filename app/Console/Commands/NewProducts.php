<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class NewProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discover:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'New products discover.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        \App\Product::chunk(500, function($products){

            foreach($products as $product) {
                $product->price = 0;
                $product->quantity = 0;
                $product->location = '';
            }

        });

        \App\ProductHasPrice::truncate();

        \App\ElitPrice::
        where('stock', '>', 0)
            ->where('price', '>', 0)
            ->chunk(500, function ($elit)
            {
                foreach ($elit as $p)

                    \App\ProductHasPrice::firstOrCreate(['product_id' => $p->product_id]);
            });

        \App\BennettPrice::where('manufacturer', '!=', '')
            ->where('price', '>', 0)
            ->where('product_id', '!=', 0)
            ->where('stock', '>', 0)
            ->chunk(500, function ($bennett)
            {
                foreach ($bennett as $p)

                    \App\ProductHasPrice::firstOrCreate(['product_id' => $p->product_id]);
            });

        \App\BardiPrice::where('stock', '>', 0)
            ->where('price', '>', 0)
            ->where('product_id', '!=', 0)
            ->chunk(500, function ($bardi)
            {
                foreach ($bardi as $p)

                    \App\ProductHasPrice::firstOrCreate(['product_id' => $p->product_id]);
            });

        \App\ConexPrice::where('stock', '>', 0)
            ->where('price', '>', 0)
            ->where('product_id', '!=', 0)
            ->chunk(500, function ($conex)
            {
                foreach ($conex as $p)

                    \App\ProductHasPrice::firstOrCreate(['product_id' => $p->product_id]);
            });

        \App\MateromPrice::where('product_id', '!=', 0)
            ->chunk(500, function ($materom)
            {
                foreach ($materom as $p)

                    \App\ProductHasPrice::firstOrCreate(['product_id' => $p->product_id]);
            });

        \App\IntercarsPrice::where('product_id', '!=', 0)
            ->chunk(500, function ($intercars)
            {
                foreach ($intercars as $p)

                    \App\ProductHasPrice::firstOrCreate(['product_id' => $p->product_id]);
            });

    }
}
