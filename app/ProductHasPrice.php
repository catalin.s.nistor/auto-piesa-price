<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductHasPrice extends Model {

    public $table = 'product_has_price';

    protected $fillable = ['product_id'];

}
