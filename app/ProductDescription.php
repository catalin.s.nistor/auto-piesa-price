<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDescription extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public $table = "product_description";

}
