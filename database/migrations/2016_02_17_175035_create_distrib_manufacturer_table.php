<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistribManufacturerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('distrib_manufacturer', function (Blueprint $table)
        {
            $table->integer('manufacturer_id')->unsigned();
            $table->integer('distrib_id')->unsigned();

            $table->foreign('manufacturer_id')
                ->references('id')
                ->on('manufacturers');

            $table->foreign('distrib_id')
                ->references('id')
                ->on('distrib');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('distrib_manufacturer');
    }
}
