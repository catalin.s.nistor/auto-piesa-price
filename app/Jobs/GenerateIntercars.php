<?php

namespace App\Jobs;

use DB;
use Storage;
use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class GenerateIntercars extends Job implements SelfHandling, ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
       $xml = new \SimpleXMLElement("<?xml version=\"1.0\" encoding=\"utf-8\" ?><items></items>");


        DB::table('distrib_manufacturer')->where('distrib_id', 6)->chunk(100, function ($mans) use ($xml)
        {
            foreach ($mans as $man)
            {
                \App\Product::where('manufacturer_id', $man->manufacturer_id)->chunk(1000, function ($products) use ($xml)
                {

                    foreach ($products as $product)
                    {
                        $xml->addChild('item', $product->model);
                    }

                });
            }
        });

        Storage::disk('public')->put('intercars.xml', $xml->asXML());

    }


}
