<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewPrice extends Model
{
    public $timestamps = false;

    protected $connection = "sqlite";

    public $guarded = [];

}
