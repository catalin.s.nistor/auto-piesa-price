<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RemoteProduct extends Model
{
    /**
     * @var string
     *
     *
     */

    public $table = "products";


    protected $connection = "sqlite";

    /**
     *
     * Use timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Fillable
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * get manufacturers
     *
     */
    public function manufacturer() {
        return $this->belongsTo('App\RemoteManufacturer', 'manufacturer_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * get categories
     *
     */
    public function categories()
    {
        return $this->belongsToMany('App\RemoteCategory', 'category_product', 'product_id', 'category_id');
    }

        /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     *
     * get product description
     *
     */
    public function description() {
        return $this->hasOne('App\RemoteProductDescription', 'id', 'id');
    }

}
