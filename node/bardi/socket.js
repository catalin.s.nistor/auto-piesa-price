var vo = require('vo');
var code = 0;


var WebSocketServer = require('ws').Server,
    wss = new WebSocketServer({port: 8081}),
    CLIENTS = [];

var code = '';
var code_needed = false;
wss.on('connection', function (ws) {
    CLIENTS[ws._socket._handle.fd] = ws;
    ws.on('message', function (message) {
        code = message;
    });

    if (code_needed)
        ws.send("code");
});

function sendAll() {
    CLIENTS.forEach(function (c, k) {
        try {
            c.send("code");
        }
        catch (err) {
            delete CLIENTS[k];
        }
    });
}

function getCode() {
    sendAll();
    code = '';
    code_needed = true;
    return new Promise(function (resolve, reject) {
        setInterval(function () {
            if (code != '')
                resolve(code);
        }, 1000);
    });
};

function * run() {


    var c = yield getCode();
    code_needed = false;
    console.log("something else");
}


setTimeout(function () {
    console.log('pause 30 sec');


    vo(run)(function (err, result) {
        if (err) throw err;
    });

}, 30000);