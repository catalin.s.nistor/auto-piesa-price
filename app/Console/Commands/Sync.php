<?php

namespace App\Console\Commands;

use Mockery\CountValidator\Exception;
use Illuminate\Console\Command;
use anlutro\cURL\Laravel\cURL;
use Carbon\Carbon;
use Storage;
use Artisan;
use Crypt;
use DB;
use SSH;

class Sync extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync new products and new prices';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $file = 'database.sqlite';

    protected $strings_replace = [' ', '-', '.', '/'];


    //protected $remote_server = 'http://p.auto-piesa.ro/';


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->downloadDB();

        if($this->getLocation() == 1)
            $this->downloadNewProducts();

        $this->set_short_model();
        $this->cleanRemoteDB();
        Artisan::call('set:prices');
        $this->uploadNewPrices();
        $this->uploadNewProducts();
        $this->setFinishedDate();
        $this->setLocation();
        $this->uploadDB();
        $this->uploadUniversal();

        $this->ping();
        
    }

    private function uploadNewPrices()
    {
        if(Storage::disk('root')->exists($this->file))
        {

            $last_45 = Carbon::today();

            $last_45->subDays(45);

            \App\Product::where('price', '!=', 0)
                ->where('updated_at', '>=', $last_45)
                ->chunk(10, function ($p)
                {
                    foreach ($p as $product)
                    {
                        $new_price = \App\NewPrice::firstOrNew([
                                'model'           => $product->model,
                                'manufacturer_id' => $product->manufacturer_id
                            ]
                        );
                        $new_price->price = $product->price;
                        $new_price->location = $product->location;
                        $new_price->save();
                    }
                });
        }
    }

    private function downloadNewProducts() //sqlite to mysql
    {
        if(Storage::disk('root')->exists('database.sqlite'))
        {
            \App\RemoteProduct::chunk(10, function ($p)
            {
                foreach ($p as $remote_product)
                {

                    $manufacturer = \App\Manufacturer::firstOrNew([
                        'id' => $remote_product->manufacturer->id,
                    ]);
                    $manufacturer->name = $remote_product->manufacturer->name;
                    $manufacturer->save();

                    $product = \App\Product::firstOrNew([
                            'model'           => $remote_product->model,
                            'manufacturer_id' => $remote_product->manufacturer_id
                        ]
                    );

                    $product->manufacturer()->associate($manufacturer);

                    $product->price = $remote_product->price;
                    $product->location = $remote_product->location;
                    $product->status = $remote_product->status;
                    $product->image = $remote_product->image;

                    echo $remote_product->model;

                    $product->save();

                    $description = \App\ProductDescription::firstOrNew([
                        'id' => $product->id,
                    ]);

                    $description->name = $remote_product->description->name;
                    $description->save();

                    foreach ($remote_product->categories as $rcategory)
                    {
                        $category = \App\Category::firstOrNew([
                            'id' => $rcategory->id,
                        ]);

                        $category->name = $rcategory->name;
                        $category->save();

                        $product->categories()->sync([$category->id]);
                    }
                }
            });
        }
    }

    private function uploadNewProducts() //mysql to sqlite
    {
        if(Storage::disk('root')->exists($this->file))
        {
            $date = $this->getLastDate();

            \App\Product::where('created_at', '>=', $date)
                ->chunk(100, function ($p)
                {
                    foreach ($p as $product)
                    {

                        $remote_manufacturer = \App\RemoteManufacturer::firstOrNew([
                            'id' => $product->manufacturer->id,
                        ]);
                        $remote_manufacturer->name = $product->manufacturer->name;
                        $remote_manufacturer->save();

                        $remote_product = \App\RemoteProduct::firstOrNew([
                                'model'           => $product->model,
                                'manufacturer_id' => $product->manufacturer_id
                            ]
                        );
                        $remote_product->manufacturer()->associate($remote_manufacturer);
                        $remote_product->location = $product->location;
                        $remote_product->status = $product->status;
                        $remote_product->price = $product->price;
                        $remote_product->image = $product->image;
                        $remote_product->save();

                        $remote_description = new \App\RemoteProductDescription();
                        $remote_description->name = $product->description->name;

                        $remote_product->description()->save($remote_description);

                        foreach ($product->categories as $category)
                        {
                            $remote_category = \App\RemoteCategory::find($category->id);

                            if(!$remote_category)
                            {
                                $remote_category = new \App\RemoteCategory();
                                $remote_category->name = $category->name;
                                $remote_category->id = $category->id;
                                $remote_category->save();
                            }

                            $remote_product->categories()->sync([$remote_category->id]);

                        }


                        $remote_product->save();
                    }

                });
        }

    }

    private function cleanRemoteDB()
    {
        \App\NewPrice::truncate();
        \App\RemoteManufacturer::truncate();
        \App\RemoteProduct::truncate();
        DB::connection('sqlite')->table('category_product')->truncate();
        \App\RemoteCategory::truncate();
        \App\RemoteProductDescription::truncate();
    }

    private function uploadUniversal()
    {
        $settings = \App\Settings::lists('value', 'key');

        $add = ($settings['ulei'] / 100) + 1;
        $universal = Storage::disk('node')->get('conex/universal.json');
        $products = [];

        foreach (json_decode($universal) as $code => $obj)
        {
            $price = tofloat($obj->price);
            $products[] = [
                "name"         => $obj->name,
                "stock"        => $obj->stock,
                "manufacturer" => $obj->manufacturer,
                "code"         => $code,
                "image"        => $obj->image,
                "price"        => ceil($price * (1 + ($settings['tva'] / 100)) * $add)
            ];
        }

        $content = json_encode($products);
        Storage::disk('root')->put('universal.json', $content);
        SSH::into('production')->put(storage_path('universal.json'), '/home/www/sync/storage/universal.json');
    }

    private function uploadDB()
    {
        SSH::into('production')->put(storage_path('database.sqlite'), '/home/www/sync/storage/database.sqlite');
    }

    private function downloadDB()
    {

        SSH::into('production')->get('/home/www/sync/storage/database.sqlite', storage_path('database.sqlite'));

        /*
        $folder = 'storage/';

        if(Storage::disk('remotedb')->exists($folder . $this->file))
        {
            $contents = Storage::disk('remotedb')->get($folder . $this->file);


            if(Storage::disk('root')->exists($this->file))
                Storage::disk('root')->delete($this->file);

            Storage::disk('root')->put($this->file, $contents);

        }
        */
    }

    private function setFinishedDate()
    {

        $date = \App\Settings::firstOrNew([
            'key' => 'updated'
        ]);

        $date->value = Carbon::now()->toDateTimeString();

        $rdate = \App\RemoteSettings::firstOrNew([
            'key' => 'updated'
        ]);

        $reset = \App\Settings::where('key', 'date_reset')->first();

        $rdate->value = Carbon::now()->toDateTimeString();

        if($reset->value == 1)
        {
            $date->value = Carbon::create(2015, 9, 1, 0, 0);

            $rdate->value = Carbon::create(2015, 9, 1, 0, 0);
            $reset->value = 0;
            $reset->save();
        }

        $date->save();
        $rdate->save();


    }

    private function getLastDate()
    {
        $date = \App\Settings::where('key', 'updated')->first();

        if(!$date)
        {
            $date = Carbon::now()->subDays(1)->toDateTimeString();

            return $date;
        }

        return $date->value;
    }

    private function getLocation()
    {
        $location = \App\RemoteSettings::where('key', 'location')->first();

        if(!$location)
            return false;

        return $location->value;

    }

    private function setLocation()
    {
        $date = \App\RemoteSettings::firstOrNew([
            'key' => 'location'
        ]);

        $date->value = 2;
        $date->save();
    }

    private function ping()
    {
        $date = Carbon::now()->toDateTimeString();

        SSH::into('production')->run([
            'cd /home/www/sync',
            'echo "' . $date . '" > test',
            'nohup php artisan sync:prices > /dev/null 2>&1 &',
        ]);

    }

    private function set_short_model()
    {

        \App\Product::chunk(500, function ($products)
        {

            foreach ($products as $product)
            {

                $product->s_model = str_replace($this->strings_replace, '', $product->model);
                $product->save();

            }

        });

    }


}