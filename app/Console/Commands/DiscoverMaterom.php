<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\MateromPrice;
use Storage;
use Excel;
use DB;

class DiscoverMaterom extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discover:materom';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Intercars distributor product disvovery.';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public $equiv = [
        'FEBI BILSTEIN'           => 'FEBI',
        'HENGST FILTER'           => 'HENGST',
        'HERTH+BUSS JAKOPARTS'    => 'JAKOPARTS',
        'LEMF╓RDER'               => 'LEMFORDER',
        'LuK'                     => 'LUK',
        'MAGNETI MARELLI'         => 'MAGNETTI MARELLI',
        'MAHLE ORIGINAL'          => 'MAHLE',
        'MANN'                    => 'MANN-FILTER',
        'Muller Filters'          => 'MULLER',
        'Saleri SIL'              => 'SIL',
        'SPIDAN'                  => 'GKN (SPIDAN)',
        'HELLA GUTMANN'           => 'HELLA',
        'INTERNATIONAL RADIATORS' => 'VAN WEZEL',
        'DACIA OE'                => 'DACIA Original',
        'RENAULT OE'              => 'RENAULT Original',

    ];

    protected $manufacturers_to_attach = [];

    protected $strings_replace = [' ', '-', '.', '/'];

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        MateromPrice::truncate();
        DB::table('distrib_manufacturer')->where('distrib_id', 6)->delete();
        
        if(Storage::disk('local')->exists('xls/materom.xlsx'))
        {
            Excel::load(storage_path('app/xls/materom.xlsx'), function ($reader)
            {

                // Getting all results
                $results = $reader->all();
                foreach ($results as $row)
                {
                    foreach ($row as $data)
                    {
                        $materom = new MateromPrice();
                        $materom->materom_code = $data->cod_npf;
                        $materom->manufacturer = $data->brand;
                        $materom->price = $data->pret;
                        $materom->stock = 100;
                        $materom->save();
                    }
                }
            });
        }
        
        \App\MateromPrice:://where("materom_code", "100390")->
        chunk(500, function ($materom)
        {
            foreach ($materom as $p)

                if($p->manufacturer != "")
                {

                    $p->ran_discover_task = 1;
                    $p->save();

                    $manufacturer = $p->manufacturer;

                    if(array_key_exists($p->manufacturer, $this->equiv))
                    {
                        $manufacturer = $this->equiv[$p->manufacturer];
                    }

                    $materom_code = $p->materom_code;

                    $product = $this->getProductId(str_replace($this->strings_replace, '', strtoupper($materom_code)), true, $manufacturer);


                    if(is_object($product))
                    {
                        $this->setProductId($product, $p);
                        continue;
                    }

                    $product = $this->getProductId(strtoupper($p->materom_code), false, $manufacturer);
                    if(is_object($product))
                    {
                        $this->setProductId($product, $p);
                        continue;
                    }
                }
        });
        
        
        \App\Manufacturer::chunk(500, function ($manufacturer)
        {
            foreach ($manufacturer as $m)
                if(in_array($m->id, $this->manufacturers_to_attach))
                {
                    $m->distribs()->attach(6);

                }
        });

        
    }

    private function setProductId($product, $materom)
    {
        $a = strpos($materom->materom_code, strtoupper($product->model));
        $b = strpos(str_replace($this->strings_replace, '', $materom->materom_code), str_replace($this->strings_replace, '', strtoupper($product->model)));

        if($a != false || $b != false || $materom->materom_code == $product->model || strtoupper(strtoupper(str_replace($this->strings_replace, '', $materom->materom_code)) == strtoupper(str_replace($this->strings_replace, '', $product->model))))
        {

            $materom->product_id = $product->id;
            $materom->model = $product->model;
            $materom->manufacturer_id = $product->manufacturer_id;
            $materom->save();

            if(!in_array($product->manufacturer->id, $this->manufacturers_to_attach))
            {
                $this->manufacturers_to_attach[] = $product->manufacturer->id;
            }

            //if($materom->stock > 0)
            \App\ProductHasPrice::firstOrCreate(['product_id' => $product->id]);
        }

    }

    private function getProductId($code, $trim = true, $manufacturer = "")
    {
        if(trim($code) == "" || strlen($code) == 0)
            return false;

        $count = \App\Product::direct($code, $trim)
            ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->where('manufacturers.name', $manufacturer)
            ->count();

        if($count == 1)
        {
            $product = \App\Product::direct($code, $trim)
                ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
                ->where('manufacturers.name', $manufacturer)
                ->first();

            return $product;
        }

        if($count != 1)
        {
            $code = substr($code, 1);

            return $this->getProductId($code, $trim, $manufacturer);
        }


    }

}
