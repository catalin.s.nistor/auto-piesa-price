<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ManufacturersController extends Controller {


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $manufacturers = \App\Manufacturer::orderBy('name')->lists('name', 'id');
        $distribs = \App\Distrib::lists('name', 'id');

        return view("pages.manufacturers", compact('manufacturers', 'distribs'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('distrib_manufacturer')->truncate();

        if($request->settings)
        {

            foreach ($request->settings as $man_id => $d)
            {
                $manufacturer = \App\Manufacturer::find($man_id);
                if(!empty($d))
                    $manufacturer->distribs()->sync($d);
            }

        }
        return redirect('manufacturers');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

}
