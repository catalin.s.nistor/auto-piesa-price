<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSearchIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table)
        {
            $table->index('model');
            $table->index('location');
        });

        Schema::table('manufacturers', function (Blueprint $table)
        {
            $table->index('name');
        });

        Schema::table('product_description', function (Blueprint $table)
        {
            $table->index('name');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
