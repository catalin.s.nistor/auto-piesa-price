var Sequelize = require('sequelize'),
    equiv = require(__dirname + '/equiv/manufacturers'),
    nconf = require('nconf');

nconf.argv()
    .env()
    .file({file: __dirname + '/config.json'});

var db = nconf.get('database'),
    scraper = nconf.get('scraper'),
    connection = new Sequelize(db.name, db.user, db.password, {
        host: db.host,
        dialect: db.dialect,
       // logging: false
    }),
    models = require('sequelize-import')(__dirname + '/mysql_models', connection);



equiv.manufacturers.forEach(function (manufacturer) {
    console.log(manufacturer.id);

    models.Elit.update({
            search: 1
    },{
        where: {
            manufacturer_id: manufacturer.id,
            product_id: {$ne: 0}
        }
    });

});
