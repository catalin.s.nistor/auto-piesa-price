/**
 * Created by cata on 03/09/15.
 */
module.exports = function (sequelize, DataType) {
    return sequelize.define("Intercars", {
            id: {type: DataType.INTEGER, primaryKey: true, autoIncrement: true, unique: true},
            product_id: {type: DataType.INTEGER},
            model: DataType.STRING,
            intercars_code: DataType.STRING,
            price: DataType.STRING,
            stock: DataType.INTEGER,
            manufacturer: {type: DataType.STRING},
            manufacturer_id: {type: DataType.INTEGER},

        },
        {
            timestamps: false,
            tableName: 'intercars_prices',
            freezeTableName: true
        }
    );
}
