#!/bin/sh

cd "$(dirname "$0")"
xvfb-run -a --server-args="-screen 0 1280x1024x24" node --harmony server.js > ./output.txt