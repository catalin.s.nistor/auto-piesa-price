<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SettingsRequest;


class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {

        $settings = \App\Settings::lists('value', 'key');

        return view("pages.settings", compact('settings'));
    }

    public function store(SettingsRequest $request) {

        foreach($request->all() as $k => $v ) {
            if($k != "_token")
            {
                $setting = \App\Settings::firstOrCreate(['key' => $k]);
                $setting->value = $v;
                $setting->save();
            }
        }

        return redirect('settings');

    }
}
