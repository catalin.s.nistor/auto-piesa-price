<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/css/all.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    @yield('head')
</head>


<body role="document">

@yield("content")

<script src="/js/all.js"></script>

@yield("footer")

</body>
</html>