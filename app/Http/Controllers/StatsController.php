<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class StatsController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view("pages.stats");
    }

    public function store(Request $request)
    {

        $setting = \App\Settings::firstOrCreate(['key' => 'date_reset']);

        $setting->value = 1;
        $setting->save();

        return redirect('stats')->with('status', 'Data Resetata');

    }
}
