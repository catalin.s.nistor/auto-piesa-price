<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DiscoverBardi extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discover:bardi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bardi distributor product discovery.';

    /**
     * Equiv
     *
     * @var array
     */

    protected $equiv = [
        'AL-KO'             => 'AL- KO',
        'BOSAL-VFM'         => 'BOSAL',
        'BOSCH-UNIPOINT'    => 'BOSCH',
        'DAYCO-TEHER'       => 'DAYCO',
        'FEBI-TEHER'        => 'FEBI',
        'FEBI-PROKIT'       => 'FEBI',
        'FTE-TEXTAR'        => 'TEXTAR',
        'NTN-SNR'           => 'SNR',
        'HELLA-ALTERNATIVE' => 'HELLA',
        'REMY'              => 'DELCO REMY',
        'KK'                => 'K &amp; K',
        'MAHLE-BEHR'        => 'MAHLE',
        'MEAT&DORIA-HOFFER' => 'MEAT &amp; DORIA',
        'NGK-NTK'           => 'NGK',
        'QH FRANCE'         => 'QUINTON HAZELL',
        'RENAULT'           => 'RENAULT Original',
        'VALEO-CLASSIC'     => 'VALEO',
        'MAGNETI MARELLI'   => 'MAGNETTI MARELLI',
        'KYB'               => 'KAYABA',
        'VERNET'            => 'CALORSTAT by Vernet'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console com mand.
     *
     * @return mixed
     */
    public function handle()
    {

        \App\BardiPrice::chunk(500, function ($bardi)

        {
            foreach ($bardi as $p)

                if($p->manufacturer != "")
                {

                    $p->ran_discover_task = 1;
                    $p->save();

                    $manufacturer = $p->manufacturer;

                    if(array_key_exists($p->manufacturer, $this->equiv))
                    {
                        $manufacturer = $this->equiv[$p->manufacturer];
                    }

                    $bardi_code = $p->bardi_code;

                    if($p->manufacturer == 'LEMFÖRDER')

                        $bardi_code = $p->bardi_code . ' 01';

                    $product = $this->getProductId(str_replace([' ', '-', '.'], '', strtoupper($bardi_code)), true, $manufacturer);
                    if($product)
                    {
                        $this->setProductId($product, $p);
                        continue;
                    }

                    $product = $this->getProductId(strtoupper($p->bardi_code), false, $manufacturer);
                    if($product)
                    {
                        $this->setProductId($product, $p);
                        continue;
                    }
                }
        });
    }

    private function setProductId($product, $bardi)
    {
        $a = strpos($bardi->bardi_code, strtoupper($product->model));
        $b = strpos(str_replace([' ', '-', '.'], '', $bardi->bardi_code), str_replace([' ', '-', '.'], '', strtoupper($product->model)));

        if($a != false || $b != false || $bardi->bardi_code == $product->model || strtoupper(strtoupper(str_replace([' ', '-', '.'], '', $bardi->bardi_code)) == strtoupper(str_replace([' ', '-', '.'], '', $product->model))))
        {

            $bardi->product_id = $product->id;
            $bardi->model = $product->model;
            $bardi->manufacturer_id = $product->manufacturer_id;
            $bardi->save();

            if($bardi->stock > 0)
                \App\ProductHasPrice::firstOrCreate(['product_id' => $product->id]);
        }

    }

    private function getProductId($code, $trim = true, $manufacturer = "")
    {
        if(trim($code) == "" || strlen($code) == 0)
            return false;

        $count = \App\Product::direct($code, $trim)
            ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
            ->where('manufacturers.name', $manufacturer)
            ->count();

        if($count == 1)
        {
            $product = \App\Product::direct($code, $trim)
                ->leftJoin('manufacturers', 'manufacturers.id', '=', 'products.manufacturer_id')
                ->where('manufacturers.name', $manufacturer)
                ->first();

            return $product;
        }

        if($count != 1)
        {
            $code = substr($code, 1);

            return $this->getProductId($code, $trim, $manufacturer);
        }


    }
}
