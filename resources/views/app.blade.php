<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="/css/all.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    @yield('head')
</head>

<body role="document">
@include('partials.bardimodal')
@include("partials.nav");

<div style="padding-top: 50px;" class="container" role="main">
    @yield("content")
</div>

<script src="/js/all.js"></script>

@include('partials.bardijs')
@yield("footer")

</body>
</html>

