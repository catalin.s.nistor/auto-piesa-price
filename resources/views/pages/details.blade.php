<div class="row">
    <div class="col-md-4"><h5>Categorie</h5>

        <ul>
            @foreach($product->categories as $category)

                {{ $category->name }}

            @endforeach
        </ul>
    </div>
    <div class="col-md-8">
        <div class="col-md-4"><h5>Preturi fara adaos:</h5>
            <ul>
                <li><strong>Bardi: </strong>{{ $product->bardi->price  or "0"}} RON</li>
                <li>
                    <strong>Elit: </strong>{{ is_object($product->elit) && $product->elit->stock > 0 ? round($product->elit->price * $tva , 2) : "0" }}
                    RON
                </li>
                <li>
                    <strong>Bennett: </strong>{{ is_object($product->bennett) && $product->bennett->stock > 0 ? round($product->bennett->price * $tva, 2) : "0" }}
                    RON
                </li>
                <li>
                    <strong>Conex: </strong>{{ is_object($product->conex) && $product->conex->stock > 0 ? round($product->conex->price * $tva, 2) : "0" }}
                    RON
                </li>
                <li>
                    <strong>Intercars: </strong>{{ is_object($product->intercars) && $product->intercars->stock > 0 ? round($product->intercars->price * $tva,2) : "0" }}
                    RON
                </li>
                <li>
                    <strong>Materom: </strong>{{ is_object($product->materom) ? round($product->materom->price * $tva,2) : "0" }}
                    RON
                </li>
            </ul>
        </div>

        <div class="col-md-4"><h5>Data adaugarii</h5>
            <ul>
                <li>{{ is_object($product->bardi) ? $product->bardi->created_at : ""}}</li>
                <li>
                    {{ is_object($product->elit) ? $product->elit->created_at  : "" }}
                </li>
                <li>
                    {{ is_object($product->bennett) ? $product->bennett->created_at : "" }}
                </li>
                <li>
                    {{ is_object($product->conex) ? $product->conex->created_at : "" }}
                </li>
                <li>
                    {{ is_object($product->intercars) ? $product->intercars->created_at : "" }}
                </li>
                <li>
                    {{ is_object($product->materom) ? $product->materom->created_at : "" }}
                </li>
            </ul>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h5>Setari Distribuitori</h5>
        <p>
            @foreach($distribs as $distrib_id => $distrib)
                {!! Form::checkbox('settings', $distrib_id, in_array($distrib_id, $existing) ? true : ($product->distribs()->find($distrib_id) ? true : false), in_array($distrib_id, $existing) ? ['disabled' => 'disabled'] : []) !!} {{$distrib}}
            @endforeach
        </p>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('input:checkbox').click(function () {
            if ($(this).is(":checked"))
                $.post( '{{ url("prices/store") }}', {product_id : {{ $product->id }}, distrib_id : $(this).val(), mode : 'add' });
            else
                $.post( '{{ url("prices/store") }}', {product_id : {{ $product->id }}, distrib_id : $(this).val(), mode : 'delete'  });
        });
    });
</script>
