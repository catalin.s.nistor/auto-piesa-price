<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model {

    public $timestamps = false;

    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     * get all products
     *
     */

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function distribs()
    {
        return $this->belongsToMany('App\Distrib', 'distrib_manufacturer', 'manufacturer_id', 'distrib_id');
    }
}
